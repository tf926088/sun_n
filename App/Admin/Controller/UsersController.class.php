<?php
namespace Admin\Controller;
use Think\Controller;

class UsersController extends CommonController {
	public function index(){
		header("Loction:lists");
		//$this->display();
	
	}

	//用户信息列表 分页
	public function lists(){
		$user_list = D('Users')->getUserList();
		$this->assign('user',$user_list);
		$this->display();
		//dump($user_list);
	}

	//用户编辑
	public function edite(){

		if(IS_POST){			
			$data =array();
			$userid  = I("userid","");	
			$user= M("users"); 
	        $condition =array('userid'=>"$userid");
	        $data['verify_status']  = I("verify_status","");
	        $data['cmoney']  = I("cmoney","");
	        $data['vip']  = I("vip","");
	        
	        $user_info = $user->where(array('userid'=>$userid))->find();
	        $result = $user->where($condition)->data($data)->save();
	        /*实名认证更新积分*/
	        if ($data['verify_status'] == 2 && $user_info['verify_status'] != 2) {
	        	$file="./App/Common/Conf/config.php"; //全局配置
	            $config = include $file;
	            $score = $config['NAME_CHECK'];  //签到积分
	            /*积分写入用户表 yjmp*/
	            $res = M()->execute(" update users set integral = (integral + '$score') where userid = '$userid'");
	            /*记录log*/
				$log_data = array(
					'user_id'=>$userid,
					'integral'=>$score,
					'desc'=>'实名认证',
					'add_time'=>time()
				);
				M('integral_log')->add($log_data);
	        }
			$this->success('操作成功!',U('Users/lists'));

		

		}else{
			$uid = I("userid",0,"int"); //用I方法，处理方式int
			$user_info =M("users")->where("userid='{$uid}'")->find();
			//dump($user_info); 
			//dump($uid);
			$this->assign("user_info",$user_info);
			$this->display();		
		}
	}


	//用户删除,删除功能已经实现，比较简单，应该加上验证
		
	public function delete(){

		$uid = I('userid/d');
		M("users")->where("userid='{$uid}'")->delete();

		$this->redirect('Users/lists');	


	}

	//以下RBAC是原生
	//角色列表
		public function role(){
			$this->role = M('role')->select();
			//dump($this);
			$this->display();
		}

        //节点列表
		public function node(){
			$node = M('node')->order('sort')->select();
			$this->node = node_merge($node);
			$this->display();
		}

        //添加用户
		public function addUser(){
			$this->role = M('role')->select();
			$this->display();
		}

        //添加用户表单处理
		public function addUserHandle(){
			//用户信息
			$user = array(
				'username' => I('username'),
				'password' => I('password','','md5'),
				'logintime' => time(),
				'loginip' => get_client_ip()
			);
			
			//所属角色
			$role_user = array();
			if($uid = M('user')->add($user)){
				foreach ($_POST['role_id'] as $v){
					$role_user[] = array(
						'role_id' => $v,
						'user_id' => $uid
					);
				}
				//添加用户角色
				M('role_user')->addAll($role_user);
				$this->success('添加成功',U('Users/index'));
			}else{
				$this->error('添加失败');
			}
		}
		   //用户等级
		public function djsun(){
			$this->display();
		}


        //添加角色
		public function addRole(){
			$this->display();
		}

		public function addRoleHandle() {
			if(M('role')->add($_POST)) {
				$this->success('添加成功',U('Users/role'));
			} else {
				$this->error('添加失败');
			}
		}

        //添加节点
		public function addNode(){
			$this->pid = I('pid',0,'intval');
			$this->level = I('level',1,'intval');

			switch($this->level){
				case 1:
					$this->type = '应用';
					break;
				case 2:
					$this->type = '控制器';
					break;
				case 3:
					$this->type = '动作方法';
					break;
			}

			$this->display();
		}

		public function addNodeHanlde(){
			if(M('node')->add($_POST)){
				$this->success('添加成功',U('Users/node'));
			}else{
				$this->error('添加失败');
			}
		}

		public function access(){
			
			$rid = I('rid',0,'intval');
			$node = M('node')->order('sort')->select();

			//原有权限
			$access = M('access')->where(array('role_id' => $rid))->getField('node_id', true);
			$this->node = node_merge($node,$access);
			$this->rid = $rid;
			$this->display();
		}

        //修改权限
		public function setAccess(){
			$rid = I('rid',0,'intval');
			$db = M('access');
			//清空原权限
			$db->where(array('role_id' => $rid))->delete();

			$data = array();

            //组合新权限
			foreach($_POST['access'] as $v){
				$tmp = explode('_',$v);
				$data[] = array(
					'role_id' => $rid,
					'node_id' => $tmp[0],
					'level' => $tmp[1]
				);
			}

            //插入新权限		
			if($db->addAll($data)){
				$this->success('修改成功',U('Users/role'));
			}else{
				$this->error('修改失败');
			}
		}

			//用户充值记录列表
	public function accountList(){
		$account_list = D('Users')->getAccountList();
		$this->assign('account_list',$account_list);
		$this->display();
	}
	//用户充值记录列表
	public function account_log(){
		$account_list = D('Users')->getAccountLog();
		$this->assign('account_list',$account_list);
		//dump($account_list);
		$this->display();
	}

	//审核用户充值记录
	public function edit_aclog(){
		if (IS_POST) {
			$data =array();
			$log_id  = I("log_id","");
			$process_type  = I("process_type","");
			
			$log_model= M("user_account"); 
	        $condition =array('log_id'=>"$log_id");
	        $data['is_paid']  = I("is_paid","");
	        $log_res = $log_model->where($condition)->data($data)->save();

	        if ($data['is_paid'] > 0 && $process_type == 1) {
	        	$user_id  = I("user_id","");
				$money  = I("amount","");
				$user_info =M("users")->where("userid='{$user_id}'")->find();
	        	$new_money = $user_info['cmoney'] + $money;
	        	$user_res = M('users')->where(array('userid'=>$user_id))->data(array('cmoney'=>$new_money))->save();
	        }

	        if ($log_res && $user_res) {
	        	$this->success('操作成功!',U('Users/accountList'));
	        	exit;
	        }
	        $this->error('操作成功！',U('Users/accountList'));
		}else{
			$log_id  = I("log_id","");
			$log_info =M("user_account")->where("log_id='{$log_id}'")->find();
			$log_info['user_name'] = M('users')->where(array('userid' => $log_info['user_id']))->getField('username', false);
			$log_info['add_time_fmt'] = date('Y-m-d H:i',$log_info['add_time']);
			$this->assign("log_info",$log_info);
			$this->display();
		}	
	}

	//删除充值/提现记录(用户资金不会因此变动)
	public function del_aclog(){
		$log_id  = I("log_id","");
		$log_info =M("user_account")->where("log_id='{$log_id}'")->find();
		$user_id = $log_info['user_id'];
		$user_info =M("users")->where("userid='{$user_id}'")->find();
		//申请提现没有提现成功返还扣除的现金余额
		if ($log_info['process_type'] == 2 && $log_info['is_paid'] == 0) {
			$new_money = $log_info['amount'] + $user_info['cmoney'];
			M("users")->where(array('user_id'=>$user_id))->data(array('cmoney'=>$new_money))->save();
		}
		$res = M("user_account")->where(array('log_id'=>$log_id))->delete();
		if ($res) {
			$this->success('操作成功！',U('Users/accountList'));exit;
		}
		$this->error('操作失败！',U('Users/accountList'));
	}



	/* lkl新增，读取用户广告列表 */

	public function useradlist(){

   		//p 1是第一页

		check_ad_time();

		$page = I("p",1,"int");

		$limit =20;   //分页数 给予一个变量  用page方法

		$Ad = D('s_ad');

		//desc是降序asc升序,page方法 

		$data = $Ad->order('ad_id DESC')->page($page.','.$limit)->select();

		$count = $Ad->count();// 查询满足要求的总记录数



		$Page = new \Think\Page($count,$limit);// 实例化分页类 传入总记录数和每页显示的记录数



		$show = $Page->show();// 分页显示输出 show方法



		//返回只能有一个值，如果要两个值用数组array

		foreach($data as $k=>$v){

			if($v['type']==1){

				$data[$k]['last_date'] = '已下架';

			}elseif($v['type']==2){

				$data[$k]['last_date'] = $this->get_last_date($v['end_time']);

			}else{

				$data[$k]['last_date'] = '已过期';

			}

			$data[$k]['username'] = D('users')->field('username')->where("userid=$v[uid]")->find();

		}

		$this->assign("adlists",array('lists'=>$data,"page" =>$show));

	 

		$this->display();

	}

	

	/* lkl新增，广告上下架 */

	public function adedite(){			

		$data =array();

		$ad_id  = I("ad_id","");	

		$Ad= D("s_ad"); 

		//$condition =array('ad_id'=>"$ad_id");

		//$ad_info = $Ad->where("ad_id=$ad_id")->find();

		$type  = $data['type'] = I("type","");

		if($type == 1){

			//设置下架

			$data['end_time'] = time();//违规重置有效期

		}

		$result = $Ad->where("ad_id=$ad_id")->data($data)->save();

		if($result){

			$this->success('操作成功!',U('Users/useradlist'));

		}else{

			$this->success('操作失败!',U('Users/useradlist'));

		}

	}

	

	/* lkl新增，读取用户广告列表 */

	public function userzdlist(){

   		//p 1是第一页

		check_ad_time();

		$page = I("p",1,"int");

		$limit =20;   //分页数 给予一个变量  用page方法

		$Zd = D('s_zd');

		//desc是降序asc升序,page方法 

		$data = $Zd->order('zd_id DESC')->page($page.','.$limit)->select();

		$count = $Zd->count();// 查询满足要求的总记录数



		$Page = new \Think\Page($count,$limit);// 实例化分页类 传入总记录数和每页显示的记录数



		$show = $Page->show();// 分页显示输出 show方法



		//返回只能有一个值，如果要两个值用数组array

		foreach($data as $k=>$v){

			if($v['type']==1){

				$data[$k]['last_date'] = '已下架';

			}elseif($v['type']==2){

				$data[$k]['last_date'] = $this->get_last_date($v['end_time']);

			}else{

				$data[$k]['last_date'] = '已过期';

			}

			//获取置顶信息得详情

			$sinfo = D('slink')->where("id=$v[sid]")->find();

			$data[$k]['title'] = $sinfo['title'];

			$data[$k]['img'] = $sinfo['img'];

			$data[$k]['username'] = D('users')->field('username')->where("userid=$v[uid]")->find();

		}

		$this->assign("zdlists",array('lists'=>$data,"page" =>$show));

	 

		$this->display();

	}

	

	/* lkl新增，置顶上下架 */

	public function zdedite(){			

		$data =array();

		$sdata =array();

		$zd_id  = I("zd_id","");	

		$Zd= D("s_zd"); 

		//$condition =array('ad_id'=>"$ad_id");

		$zd_info = $Zd->where("zd_id=$zd_id")->find();

		$type  = $data['type'] = I("type","");

		if($type == 1){

			//设置下架

			$data['end_time'] = time();//违规重置有效期

			$sdata['zd_end_time'] = time();//违规重置有效期

			$sdata['sort'] = 0;//违规重置有效期

		}

		$result = $Zd->where("zd_id=$zd_id")->data($data)->save();

		if($result){

			D('slink')->where("id=$zd_info[sid]")->data($sdata)->save();

			$this->success('操作成功!',U('Users/userzdlist'));

		}else{

			$this->success('操作失败!',U('Users/userzdlist'));

		}

	}

	

	

	

	

	

	

	

	

	

	

	

	

	



	//删除资金记录

	public function delete_aclog(){

		$log_id  = I("log_id");

		$res = M("user_account_log")->where(array('log_id'=>$log_id))->delete();

		$this->success('操作成功！',U('Users/account_log'));

	}

	

	//lkl读取剩余时间

	function get_last_date($time){

		$now = time();

		$last_time = $time - $now;



		if($last_time>0){

			$day = floor($last_time/24/3600);

			$hour = floor(($last_time-$day*24*3600)/3600);

		}

		return "剩余{$day}天{$hour}小时";

	}
}		



		