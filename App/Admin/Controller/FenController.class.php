<?php
namespace Admin\Controller;
use Think\Controller;
header("Content-type: text/html; charset=utf-8"); 
class FenController extends CommonController 
{
    public function index()
    {
	    $dist_list = M('distribution_conf');
	    $data = $dist_list->select();
	    $this->assign('data',$data);
        $this->display();
    }

    public function set_distribution(){
    	$post_data = I();
    	unset($post_data['submit']);

    	foreach ($post_data as $k => $v) {
    		$data = array(
    			'd_value'=>$v
    			);

    		$where = array(
    			'd_key'=>$k
    			);
    		M('distribution_conf')->where($where)->data($data)->save();
    	}
    	$this->success('操作完成！');
    	exit;
    }

    public function vip_set(){
        if(IS_POST){
            $file='./App/Common/Conf/config.php';
            $config=array_merge(include $file,array_change_key_case($_POST,CASE_UPPER));

            $str="<?php\r\nreturn ".var_export($config,true)."; ?>";
            if(file_put_contents($file, $str)){
                $this->success('修改配置成功',U('vip_set'));
            }else{
                $this->error('修改配置失败');
            }
            return;
        }
        $this->display();
    }

   












}