<?php
namespace Admin\Controller;
use Think\Controller;

class JobController extends CommonController{
	
	//散工信息列表 分页
	public function lists(){
		$job_list = D("Job")->getJobList();
		$this->assign('job',$job_list);
		$this->display();
		//dump($job_list);

		

	}



	//添加散工信息
	public function add(){

		if(IS_POST){

			$upload = new \Think\Upload();// 先实例化上传类 
			$upload->maxSize   = 3145728 ;// 设置附件上传大小 
			$upload->exts      = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型    
			$upload->savePath  = './logo/'; // 设置附件上传目录  
			$upload->rootPath  = './Upload/';
			// 上传文件     
			$info   =   $upload->upload();  

			echo"<pre>";
			print_r($info);

			if(!$info) {
			// 上传错误提示错误信息        
			$this->error($upload->getError());
			}else{
			// 上传成功        
			$this->success('上传成功！');  
			}

		
		    //dump($_POST);	
			//左边是数据库，右边是传过来的
			$data =array();
			$data['title']=I("title","",'');
			//注意二位数组读取方式
			$lu =date("Y-m-d");
			$data['logo']="$lu/".$info['logo']['savename'];
			$data['money']=I("money",0,'float');   //散工工资
			$data['number']=I("number"); //招聘人数
			$data['trait']=I("trait"); //招聘职位类型
			$data['mobile']=I("mobile");
			$data['email']=I("email");
			$data['address']=I("address");
			$data['companyname']=I("companyname","",''); //公司名称

			$des  = html_entity_decode($_POST[codeDes]);//html_entity_decode   htmlspecialchars
			$des2 = html_entity_decode($_POST[codeDes]);
			$des3 = html_entity_decode($_POST[codeDes]);
			$des4 = html_entity_decode($_POST[codeDes]);

			$data['des']=I("editor",'');  //招聘内容
			$data['des2']=I("editor2",'');
			$data['des3']=I("editor3",'');
			$data['des4']=I("editor4",'');
			
			$data['label1']=I("label1");
			$data['label2']=I("label2");
			$data['label3']=I("label3");
			$data['label4']=I("label4");
			$data['label5']=I("label5");
			$data['label6']=I("label6");
			$data['label7']=I("label7");
			$data['label8']=I("label8");
			
			$data['create_time'] = NOW_TIME;

			D("Job")->add($data);
			dump($data);
		
			//header("Location:".U("Job/lists"));

		}else{

		$this->display();
		}
	}

//散工信息编辑
	public function edite(){
		if(IS_POST){
			$data =array();
			$data['title'] = I("title","");
			$data['money']  = I("money","");
			$uid=I('get.id/d'); // 如果不存在$_GET['id'] 则返回0
			M("job")->where("id ='{$uid}'")->save($data);

			//dump($data);
			//dump($uid);

			$this->redirect('Job/lists');
		


		}else{
			$uid = I("id",0,"int");
			$job_info =M("job")->where("id='{$uid}'")->find();
			//dump($user_info);
			//dump($uid);
			$this->assign("job_info",$job_info);
			$this->display();		
		}
	}




/*	public function upload(){  
		  $upload = new \Think\Upload();// 实例化上传类 
		  $upload->maxSize   = 3145728 ;// 设置附件上传大小 
		  $upload->exts      = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型    
		  $upload->savePath  = './logo/'; // 设置附件上传目录    
		  // 上传文件     
		  $info   =   $upload->upload();    
		  if(!$info) {
		  // 上传错误提示错误信息        
		  $this->error($upload->getError());
		  }else{
		  // 上传成功        
		  $this->success('上传成功！');  

		  $model = M('Job');// 取得成功上传的文件信息
		  dump($model);
		  $info = $upload->upload();// 保存当前数据对象
		  $data['logo'] = $info[0]['logo'];
		  $data['create_time'] = NOW_TIME;
		  $model->add($data);  
		}
	}
*/


	//散工信息删除,已经实现
	public function delete(){
		$uid = I('id/d');
		M("job")->where("id='{$uid}'")->delete();
		//dump($uid);
		$this->redirect('Job/lists');	


	}	

	
}