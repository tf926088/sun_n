<?php

namespace Admin\Controller;
use Think\Controller;


/**
 * 问题分类控制器
 */
Class CateController extends CommonController {

    //分类列表视图 所有分类
    public function lst()
    {
        $cate=D('cate');
        $cates=$cate->catetree();  //执行了一个catetree 的方法
        //dump($cate);
        $this->assign('cates',$cates);
        $this->display();
    }

        //网页栏分类列表
    public function web()
    {
        $cate=D('cate');

        $cates=$cate->catetreeweb($data);
        
        //dump($cates);  
        $this->assign('cates',$cates);

        $this->display();
    }

            //
    public function enc()
    {
      
        $this->display();
    }
            //
    public function news()
    {
      
        $this->display();
    }

//分类添加
 public function add()
    {
        if(IS_POST){
            $cate=D('cate');
            $data['pid']=I('pid');
            $data['name']=I('name');
            $data['type']=I('type');
            $data['class']=I('class');
            $data['keywords']=I('keywords');
            $data['des']=I('des');
            $data['pc']=I('pc');
            $data['content']=I('content');
    

            if($cate->create($data))
            {
                if($cate->add())
                {
                    $this->success('新增栏目成功！',U('lst'));
                }else
                {
                    $this->error('新增栏目失败！');
                }

            }else
            {
                $this->error($cate->getError());
            }

            return;
        }

        //第二部分    
        $cate=D('cate');
        $cates=$cate->catetree();
        $this->assign('cates',$cates);
        $this->display();
    }

//分类编辑
    public function edit()
    {

        $cate=D('cate');
        if(IS_POST)
       {
            $data['pid']=I('pid');
            $data['name']=I('name');
            $data['id']=I('id');
            $data['type']=I('type');
            $data['class']=I('class');
            $data['pc']=I('pc');
            $data['keywords']=I('keywords');
            $data['des']=I('des');
            $data['content']=I('content');
 

            if($cate->create($data))
            {
                if($cate->save())
                {
                    $this->success('修改栏目成功！',U('lst'));
                }else
                {
                    $this->error('修改栏目失败！');
                }

            }else
            {
                $this->error($cate->getError());
            }

            return;
        }

        $cateid=I('id');
        $cateres=$cate->find($cateid);
        $this->assign('cateres',$cateres);
        $cates=$cate->catetree();
        $this->assign('cates',$cates);
        $this->display();

    }

//分类删除
    public function del()
    {
        $cate=D('cate');
        $id=I('id');
        if($cate->delete($id))
        {
            $this->success('成功删除栏目！',U('lst'));
        }else
        {
            $this->error('删除栏目失败！');
        }
    }
//分类批量删除
    public function bdel()
    {
        $cate=D('cate');
        $ids=I('ids');
        $ids=implode(',', $ids);  //1,2,3,4
        if($ids)
        {
            if($cate->delete($ids))
            {
                $this->success('批量删除栏目成功！',U('lst'));
            }else
            {
                $this->error('批量删除栏目失败！');
            }
        }else
        {
            $this->error('未选中任何内容！');
        }
    }

//分类排序
    public function sortcate()
    {
        $cate=D('cate');
        foreach ($_POST as $id=>$sort) {
            $cate->where("id=$id")->setField('sort',$sort);
        }

        $this->success('成功更新栏目顺序！',U('lst'));
    }




}