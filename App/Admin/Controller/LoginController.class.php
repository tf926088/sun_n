<?php
namespace Admin\Controller;
use Think\Controller;

//后台登录控制器
class LoginController extends Controller {

       public function index(){
        if(IS_POST){

            $admin=D('admin');
            if($admin->create($_POST,4)){
                if($admin->login()){
                    $this->success('登录中...',U('index/index'));
                }else{
                    $this->error('用户名或密码错误！');
                }
            }else{
                $this->error($admin->getError());
            }
            return;
        }
        $this->display();
    }

   

	/**
     * 验证码
     */
    public function verify(){
        $Verify = new \Think\Verify();
        $Verify->length   = 4;
        $Verify->entry();
    }
     /**
     * 检测验证码.与官方例子基本相同
     */
     function check_verify($verify, $id = 1) {
        $verify = new Verify();

        return $verify->check($verify, $id);
    }

}