<?php

namespace WishAdmin\Controller;
use Think\Controller;
class MsgManageController extends CommonController {

    Public function index(){

        $count = M('wish')->count();

        $page = new \Think\Page($count ,10);
        $limit = $page->firstRow . ',' . $page->listRows;

        $wish = M('wish')->order('time DESC')->limit($limit)->select();
        $this->wish = $wish;
        $page->setConfig('theme', '<span class="rows">共 %TOTAL_ROW% 条记录</span> %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
        $this->page = $page->show();
        $this->display();
    }

    Public function delete(){
        $id = I('id','','intval');

        if(M('wish')->delete($id)){
            $this->success('删除成功',U('MsgManage/index'));
        }else{
            $this->error('删除失败');
        }
    }
	
}
