<?php
namespace Admin\Controller;
use Think\Controller;

class ProjectController extends CommonController{

	//网赚项目列表
	public function lists(){
		$project_list = D("Project")->getProjectList();
		$this->assign('project_list',$project_list);


		$this->display();

	}

//添加网赚项目的add方法
	public function add(){
		if(IS_POST){

      $upload = new \Think\Upload();// 先实例化上传类 
      $upload->maxSize   = 3145728 ;// 设置附件上传大小 
      $upload->exts      = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型    
      $upload->savePath  = './logo/'; // 设置附件上传目录  
      $upload->rootPath  = './Upload/';
      // 上传文件     
      $info   =   $upload->upload();  

      dump($info);

      if(!$info) {
      // 上传错误提示错误信息        
      $this->error($upload->getError());
      }else{
      // 上传成功        
      $this->success('上传成功！');  
      }



    
			$data =array();
			$data['title']  = I("title","",'');
      $data['money']  = I("money","");
      $data['company_name']  = I("companyname","");
      $data['ades']  = I("ades","");
      $data['link']  = I("link","");
      $data['money']  = I("money","");
      $lu =date("Y-m-d");
      $data['img']="$lu/".$info['img']['savename'];

      $exp_rule = html_entity_decode($_POST[codeDes]);//html_entity_decode   htmlspecialchars
      $exp_award = html_entity_decode($_POST[codeDes]);
      $exp_str = html_entity_decode($_POST[codeDes]);
      $exp_reg = html_entity_decode($_POST[codeDes]);

      $data['exp_rule']=I("exp_rule",'');  //网赚内容
      $data['exp_award']=I("exp_award",'');  //网赚内容
      $data['exp_str']=I("exp_str",'');  //网赚内容
      $data['exp_reg']=I("exp_reg",'');  //网赚内容
      
      

      $data['create_time'] = NOW_TIME;
      dump($data);

			D("Project")->add($data);
		
			//header("Location:".U("Project/lists"));

		}else{
      
		$this->display();
		}
	}

    //优惠券 列表
  public function coulists(){
    $coupon_list = D("Project")->getcouponlist();
    $this->assign('coupon_list',$coupon_list);
    //dump($coupon_list);
    $this->display();

  }

  //添加优惠券  项目的add方法
  public function addcou(){
    if(IS_POST){

      $upload = new \Think\Upload();// 先实例化上传类 
      $upload->maxSize   = 3145728 ;// 设置附件上传大小 
      $upload->exts      = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型    
      $upload->savePath  = './logo/'; // 设置附件上传目录  
      $upload->rootPath  = './Upload/';
      // 上传文件     
      $info   =   $upload->upload();  

      dump($info);

      if(!$info) {
      // 上传错误提示错误信息        
      $this->error($upload->getError());
      }else{
      // 上传成功        
      $this->success('上传成功！');  
      }


      $data =array();
      $data['title']  = I("title","",'');
      $data['money']  = I("money","");
      $data['company_name']  = I("companyname","");
      $data['ades']  = I("ades","");
      $data['link']  = I("link","");
      $data['money']  = I("money","");
      $lu =date("Y-m-d");
      $data['img']="$lu/".$info['img']['savename'];

      $exp_rule = html_entity_decode($_POST[codeDes]);//html_entity_decode   htmlspecialchars
      $exp_award = html_entity_decode($_POST[codeDes]);
      $exp_str = html_entity_decode($_POST[codeDes]);
      $exp_reg = html_entity_decode($_POST[codeDes]);

    
      
      

      $data['create_time'] = NOW_TIME;
      dump($data);

      D("s_coupon")->add($data);
    
      header("Location:".U("Project/coulists"));

    }else{
      
    $this->display();
    }
  }


  //优惠券信息编辑
    public function couedite(){
      if(IS_POST){
        $data =array();
        $data['title'] = I("title","");
        $data['money']  = I("money","");
        $data['company_name']  = I("companyname","");
        $data['ades']  = I("ades","");
        $data['link']  = I("link","");
        $data['exp_rule']  = I("exp_rule","");
        $uid=I('get.id/d'); // 如果不存在$_GET['id'] 则返回0
        M("s_coupon")->where("id ='{$uid}'")->save($data);

        //dump($data);
        //dump($uid);

        $this->redirect('Project/coulists');
      


      }else{
        $uid = I("id",0,"int");
        $project_info =M("s_coupon")->where("id='{$uid}'")->find();
        //dump($project_info);
        //dump($uid);
        $this->assign("coupon_info",$coupon_info);
        $this->display();   
      }
    }



	//改变Ueditor 默认图片上传路径
        public function checkPic(){
            import('ORG.Net.UploadFile');
             $upload = new UploadFile();// 实例化上传类
             $upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
             $upload->autoSub =true ;
             $upload->subType ='date' ;
             $upload->dateFormat ='ym' ;
             $upload->savePath =  './Upload/img/';// 设置附件上传目录
             if($upload->upload()){
                 $info =  $upload->getUploadFileInfo();
                 echo json_encode(array(
                   'url'=>$info[0]['savename'],
                   'title'=>htmlspecialchars($_POST['pictitle'], ENT_QUOTES),
                   'original'=>$info[0]['name'],
                   'state'=>'SUCCESS'
                 ));
              }else{
                 echo json_encode(array(
                  'state'=>$upload->getErrorMsg()
                 ));
                     }

            }

  //网赚信息编辑
    public function edite(){
      if(IS_POST){
        $data =array();
        $data['title'] = I("title","");
        $data['money']  = I("money","");
        $data['company_name']  = I("companyname","");
        $data['ades']  = I("ades","");
        $data['link']  = I("link","");
        $data['exp_rule']  = I("exp_rule","");
        $uid=I('get.id/d'); // 如果不存在$_GET['id'] 则返回0
        M("project")->where("id ='{$uid}'")->save($data);

        //dump($data);
        //dump($uid);

        $this->redirect('Project/lists');
      


      }else{
        $uid = I("id",0,"int");
        $project_info =M("project")->where("id='{$uid}'")->find();
        //dump($project_info);
        //dump($uid);
        $this->assign("project_info",$project_info);
        $this->display();   
      }
    }



    //散工信息删除,已经实现
    public function delete(){
      $uid = I('id/d');
      M("project")->where("id='{$uid}'")->delete();
      //dump($uid);
      $this->redirect('Project/lists');  


    } 

        //优惠券信息删除
    public function coudelete(){
      $uid = I('id/d');
      M("s_coupon")->where("id='{$uid}'")->delete();
     dump($uid);
      $this->redirect('Project/coulists');  


    } 






}
	