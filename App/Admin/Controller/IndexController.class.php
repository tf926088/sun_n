<?php
namespace Admin\Controller;
use Think\Controller;

//后台首页视图
class IndexController extends CommonController {

       //后台首页视图
    public function index(){
      
        $this->display();
    }



    //后台主页，所有管理员退出
    public function logout(){

        //卸载session
        session_unset();
        //销毁session
        session_destroy();

        //删除用于自动登录的cookie
        @setcookie("auto",'',time() - 3600,'/');
        $this->redirect('Login/index');

    }


 

}