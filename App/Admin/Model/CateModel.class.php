<?php
namespace Admin\Model;
use Think\Model;
class CateModel extends Model
{
	protected $_validate = array(
		array('name','require','栏目名称不得为空！',1),  // 都有时间都验证
		array('name','','栏目名称不得重复！',1,unique,1), 
		);

	public function catetree()
	{

		$data=$this->order('sort asc')->select(); //搜索所有
		return $this->resort($data);
	}

		public function catetreeweb() //@kw    只列出pid=240的数据 
	{

		$data=$this->where('pid=240')->select(); //搜索所有
		dump($data);//获取到了数据  $data是一个二位数组
		return $this->resort($data);
	}



	public function resort($data,$parentid=0,$level=0)   //resort 私有的方法
	{
		//递归对所有分类重新排序
		static $ret=array();  // 定义一个静态数组
		foreach ($data as $k => $v) 
		{
			if($v['pid']==$parentid)
			{
				$v['level']=$level;
				$ret[]=$v;
				$this->resort($data,$v['id'],$level+1);
			}
		}

		return $ret;

	}

    //子分类，childid
	public function childid($cateid)
	{
		$data=$this->select();
		return $this->getchildid($data,$cateid);
	}

	//获取子id
	public function getchildid($data,$parentid)
	{
		static $ret=array();
		foreach ($data as $k=>$v) {
			if($v['pid']==$parentid)
			{
				$ret[]=$v['id'];
				$this->getchildid($data,$v['id']);
			}
		}

		return $ret;
	}


	public function _before_delete($options)
	{
		//单独删除时候id的值，是一个字符串，是一个单独的id
		//$options['where']['id']    int(5)
		if(is_array($options['where']['id']))
		{
			$arr=explode(',', $options['where']['id'][1]);
			$soncates=array();
			foreach ($arr as $k=>$v)
			{
				$soncates2=$this->childid($v);
				$soncates=array_merge($soncates,$soncates2);
			}
			$soncates=array_unique($soncates);
			$chilrenids=implode(',', $soncates);

		}else
		{
			$chilrenids=$this->childid($options['where']['id']);
			$chilrenids=implode(',', $chilrenids);
			
		}

		if($chilrenids)
			{
				$this->execute("delete from cate where id in($chilrenids)");
			}
	}

}