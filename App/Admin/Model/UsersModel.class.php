<?php
namespace Admin\Model;
use Think\Model;
class UsersModel extends Model{
	//用户分页
	public function getUserList(){
   		
   		//p 1是第一页
		$page = I("p",1,"int");
		$limit =17;   //分页数 给予一个变量  用page方法

		//desc是降序asc升序,page方法 
		$data = $this->order('userid DESC')->page($page.','.$limit)->select();
		$count = $this->count();// 查询满足要求的总记录数

		$Page = new \Think\Page($count,$limit);// 实例化分页类 传入总记录数和每页显示的记录数

		$show = $Page->show();// 分页显示输出 show方法

		//返回只能有一个值，如果要两个值用数组array
		return array("lists" =>$data,"page" =>$show); 
	}


		//用户充值提现记录 yjmp
	public function getAccountList(){
   		
   		//p 1是第一页
		$page = I("p",1,"int");
		$limit =17;   //分页数 给予一个变量  用page方法

		//desc是降序asc升序,page方法 
		$data = D('user_account')->order('log_id DESC')->page($page.','.$limit)->select();
		$count = D('user_account')->count();// 查询满足要求的总记录数

		$Page = new \Think\Page($count,$limit);// 实例化分页类 传入总记录数和每页显示的记录数

		$show = $Page->show();// 分页显示输出 show方法

		foreach ($data as $key => $value) {
			$data[$key]['user_name'] = M('users')->where(array('userid' => $value['user_id']))->getField('username', false);
			$data[$key]['add_time_fmt'] = date('Y-m-d H:i',$value['add_time']);
		}
		//返回只能有一个值，如果要两个值用数组array
		return array("lists" =>$data,"page" =>$show); 
	}

	/*用户资金记录*/
	public function getAccountLog(){
		//p 1是第一页
		$page = I("p",1,"int");
		$limit =17;   //分页数 给予一个变量  用page方法

		//desc是降序asc升序,page方法 
		$data = D('user_account_log')->order('log_id DESC')->page($page.','.$limit)->select();
		$count = D('user_account_log')->count();// 查询满足要求的总记录数




		$Page = new \Think\Page($count,$limit);// 实例化分页类 传入总记录数和每页显示的记录数

		$show = $Page->show();// 分页显示输出 show方法

		foreach ($data as $key => $value) {
			$data[$key]['to_user'] = M('users')->where(array('userid' => $value['user_id']))->getField('username', false);
			if ($value['from_user'] != 0) {
				$data[$key]['from_user_name'] = M('users')->where(array('userid' => $value['from_user']))->getField('username', false);
			}
			$data[$key]['add_time_fmt'] = date('Y-m-d H:i',$value['add_time']);

		}
		//返回只能有一个值，如果要两个值用数组array
		return array("lists" =>$data,"page" =>$show); 
	}

}	