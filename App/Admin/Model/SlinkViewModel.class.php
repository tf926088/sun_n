<?php
namespace Admin\Model;
use Think\Model\ViewModel;
class SlinkViewModel extends ViewModel
{
	public $viewFields = array(
		'slink'=>array('id','title','rem','pic','_type'=>'LEFT'),
		'cate'=>array('name','_on'=>'slink.cateid=cate.id')
		);
	
}