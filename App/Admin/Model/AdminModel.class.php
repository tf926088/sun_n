<?php
namespace Admin\Model;
use Think\Model;
class AdminModel extends Model
{
	protected $_validate = array(
		array('name','require','管理员名称不得为空！',1),  // 都有时间都验证
		array('name','','管理员名称不得重复！',1,unique,1), 
		array('name','','管理员名称不得重复！',1,unique,2),
		array('pwd','require','管理员密码不得为空！',1),
		array('verify','verify','验证码错误！',1,'callback',4),
		);


	/**
	 * [getpri description]
	 * @param  [type] $roleid [description]
	 * @return [type]         [description]
	 */
	public function getpri($roleid){  //获取权限
		$role=D('role');
		$pri=D('privilege');
		$role->field('rolename,pri_id_list')->find($roleid);
		session('rolename',$role->rolename);
		if($role->pri_id_list=='*'){
			session('privilege','*');
			$menu=$pri->where("parentid=0")->select();
			foreach ($menu as $k => $v) {
				$menu[$k]['sub']=$pri->where('parentid='.$v['id'])->select();
			}

			session('menu',$menu);

		}else{
			//Admin/Admin/add,Admin/Article/add  分隔数据
			
			$pris=$pri->field('id,parentid,pri_name,mname,cname,aname,CONCAT(mname,"/",cname,"/",aname) url')->where("id IN({$role->pri_id_list})")->select();
			//二维数组转为一维数组，
			$_pris=array();//先准备个空数组
			$menu=array();
			foreach($pris as $k=>$v){
				$_pris[]=$v['url'];  //取个别名
				if($v['parentid']==0){
				$menu[]=$v;
				}
			}
			session('privilege',$_pris);
			foreach ($menu as $k => $v) {
				foreach ($pris as $k1 => $v1) {
					if($v1['parentid']==$v['id']){
						$menu[$k]['sub'][]=$v1;
					}
					
				}
			}

			session('menu',$menu);
		}

	}


	public function login(){
		$pwd=$this->pwd;
		$info=$this->where("name='$this->name'")->find();
		if($info){
			$id=$info['id'];
			//$roles=$this->field('b.rolename')->alias('a')->join('LEFT JOIN cs_role b ON a.roleid=b.id')->where("a.id=$id")->find();
			$rolename=$roles['rolename'];
			if($info['pwd']==md5($pwd)){
				session('id',$info['id']);
				session('name',$info['name']);
				//session('rolename',$rolename);
				$this->getpri($info['roleid']);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function verify($code){
		$verify = new \Think\Verify();
		return $verify->check($code, '');
	}



	




























}