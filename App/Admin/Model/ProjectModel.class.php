<?php
namespace Admin\Model;
use Think\Model;
class ProjectModel extends Model{

	//添加网赚项目
	public function addProject($data){
		$data['create_time']=time();
		$data['updata_time']=time();

		return $this->add($data);
	}

	public function projectLists(){
		return $this->select();
	}

	//网赚项目分页
	public function getProjectList(){
   
		$page = I("p",1,"int");
		$limit =15;   //分页数 给予一个变量
		$data = $this->order('create_time DESC')->page($page.','.$limit)->select();//desc是降序asc升序

		$count = $this->count();// 查询满足要求的总记录数

		$Page = new \Think\Page($count,$limit);// 实例化分页类 传入总记录数和每页显示的记录数

		$show = $Page->show();// 分页显示输出

		return array("lists" =>$data,"page" =>$show); //返回只能有一个值，如果要两个值用数组array
	}


    //添加优惠券
	public function addcou($data){
		$data['create_time']=time();
		$data['updata_time']=time();

		return $this->add($data);
	}


	//优惠券列表

	public function coulists(){
		return $this->select();
	}

	//优惠券分页
	public function getcouponlist(){
   
		$page = I("p",1,"int");
		$limit =15;   //分页数 给予一个变量
		$data = $this->order('create_time DESC')->page($page.','.$limit)->select();//desc是降序asc升序

		$count = $this->count();// 查询满足要求的总记录数

		$Page = new \Think\Page($count,$limit);// 实例化分页类 传入总记录数和每页显示的记录数

		$show = $Page->show();// 分页显示输出

		return array("coulists" =>$data,"page" =>$show); //返回只能有一个值，如果要两个值用数组array
	}
}