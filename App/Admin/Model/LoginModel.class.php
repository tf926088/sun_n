<?php

namespace Admin\Model;
use Think\Model;

class LoginModel extends Model {
    // 重新定义表
    protected $tableName = 'admin';

    //系统支持数据的批量验证功能，只需要在模型类里面设置patchValidate属性为true（ 默认为false），
    protected $patchValidate = true;

    /**
     * 自动验证
     * self::EXISTS_VALIDATE 或者0 存在字段就验证（默认）
     * self::MUST_VALIDATE 或者1 必须验证
     * self::VALUE_VALIDATE或者2 值不为空的时候验证
     */
    protected $_validate = array(
       
        //验证用户名
        array('name', 'require', '用户名不能为空！',self::MUST_VALIDATE, 'regex', self::MODEL_BOTH), //默认情况下用正则进行验证
       
        array('name', '3,32', '用户名长度为1-32个字符', self::MUST_VALIDATE, 'length', self::MODEL_BOTH),
        array('name', '', '用户名被占用', self::MUST_VALIDATE, 'unique', self::MODEL_BOTH),
        array('name', '/^(?!_)(?!\d)(?!.*?_$)[\w\一-\龥]+$/', '用户名只可含有数字、字母、下划线且不以下划线开头结尾，不以数字开头！', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),

        array('pwd', 'require', '登录密码不能为空！'), // 默认情况下用正则进行验证
        //array('password2', 'require', '密码不正确！'), // 密码二次验证
        array('verify', 'verify_check', '验证码错误', 0, 'function'), // 判断验证码是否正确
    );

    /**
     * 自动完成
     */
    protected $_auto = array (
        /* 登录的时候自动完成 */
        array('pwd', 'md5', 3, 'function') , // 对password字段使用md5函数处理

        array('last_time', 'time', 1, 'function'), // 对lastdate字段在登录的时候写入当前时间戳
        array('login_ip', 'get_client_ip', 1, 'function'), // 对lastip字段在登录的时候写入当前登录ip地址
    );
}