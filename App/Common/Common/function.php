<?php
	//自定义打印函数pr 
    function pr($content) {
        echo '<pre/>';
        dump($content,true,'',false);
    }

	
	/**
	 * 递归重新排序无限级分类数组，先进后出
	 */
	function recursive ($array, $pid=0, $level=0) {  //$array 我们接受到的数组
		$arr = array();

		//循环这个数组
		foreach ($array as $v) {
			if ($v['pid'] == $pid) {
				$v['level'] = $level;
				$v['html'] = str_repeat('--', $level);
				$arr[] = $v;
				$arr = array_merge($arr, recursive($array, $v['id'], $level + 1));
			}
		}

		return $arr;
	}


	/**
	 * 获取传递ID的所有子分类ID
	 * @param  [type] $array [description]
	 * @param  [type] $id    [description]
	 * @return [type]        [description]
	 * 先进后出
	 */
	function get_all_child ($array, $id) {
		$arr = array();

		foreach ($array as $v) {
			if ($v['pid'] == $id) {
				$arr[] = $v['id'];
				$arr = array_merge($arr, get_all_child($array, $v['id']));
			}
		}

		return $arr;
	}


	function SendMail($address,$title,$message)
		{
		Vendor('PHPMailer.PHPMailerAutoload');
		//require_once(".plugin/phpMailer/class.phpmailer.php"); //sun 邮寄地址在plugin里
		$mail=new PHPMailer();
		// 设置PHPMailer使用SMTP服务器发送Email
		$mail->IsSMTP();
		// 设置邮件的字符编码，若不指定，则为'UTF-8'
		$mail->CharSet='UTF-8';
		// 添加收件人地址，可以多次使用来添加多个收件人
		$mail->AddAddress($address);
		// 设置邮件正文
		$mail->Body=$message;
		// 设置邮件头的From字段。
		$mail->From=C('MAIL_ADDRESS');
		// 设置发件人名字
		$mail->FromName='太阳网';
		// 设置邮件标题
		$mail->Subject=$title;
		// 设置SMTP服务器。
		$mail->Host=C('MAIL_SMTP');
		// 设置为“需要验证”
		$mail->SMTPAuth=true;
		// 设置用户名和密码。
		$mail->Username=C('MAIL_LOGINNAME');
		$mail->Password=C('MAIL_PASSWORD');
		// 发送邮件。
		return($mail->Send());
		}

	/*发送邮件 yjmp*/	
	function yj_send_mail($to, $title, $content){
		Vendor('PHPMailer.PHPMailerAutoload');     
        $mail = new PHPMailer(); //实例化
        $mail->IsSMTP(); // 启用SMTP
        $mail->Host=C('MAIL_HOST'); //smtp服务器的名称（这里以QQ邮箱为例）
        $mail->SMTPAuth = C('MAIL_SMTPAUTH'); //启用smtp认证
        $mail->Username = C('MAIL_USERNAME'); //你的邮箱名
        $mail->Password = C('MAIL_PASSWORD') ; //邮箱密码
        $mail->From = C('MAIL_FROM'); //发件人地址（也就是你的邮箱地址）
        $mail->FromName = C('MAIL_FROMNAME'); //发件人姓名
        $mail->AddAddress($to,"尊敬的");
        $mail->WordWrap = 50; //设置每行字符长度
        $mail->IsHTML(C('MAIL_ISHTML')); // 是否HTML格式邮件
        $mail->CharSet=C('MAIL_CHARSET'); //设置邮件编码
        $mail->Subject =$title; //邮件主题
        $mail->Body = $content; //邮件内容
        $mail->AltBody = "这是"; //邮件正文不支持HTML的备用显示
        return($mail->Send());
	}

	/*生成随机字符串 yjmp*/
	function yj_rand_str($len){
		$str = "1234567890abcdefghijklmnopqrstuvwxyz";
		$res = '';
		for ($i=0; $i < $len; $i++) { 
			$res .= $str[rand(0,35)];
		}
		return $res;
	}



//运费计算接口
function yfjs($tweight,$peisong,$province,$city,$county){
	if($peisong=='顺丰'){  //配送
		return 15;
	}else{
		return 20;
	}
}

//会员签到
function getSign($row) {
    $t = $row + 1;
    if ($t > date('d')) {
        $td = "<td style='background-color:lemonchiffon' valign='top'>
<div align='right' valign='top'><span style='position:relative;right:20px;'>" . $t . "</span>
</div><div align='left'> </div><div align='left'> </div></td>";
    } else {
        if (strlen($t) == 1) {
            $day = "0" . $t;
        } else {
            $day = $t;
        }
        $t2 = strtotime(date("Y-m-" . $day . ""));
        $info = M("s_sign")->field("id")->where("addtime = " . $t2 . " AND status = 0 AND uid = " . session('uid') . "")->find();
        if ($info) {
            $td = "<td style='background-color:navajowhite;navajowhite ;'>
<div align='right' valign='top'><span style='position:relative;right:20px;'>" . $t . "</span>
</div><div align='left'>
<img width='35px' height='35px' src='/Public/Home/images/k/gou.png' style='position:relative;left:10px;'> 已签到
</div></td>";
        } else {
            if ($t == date('d')) {
                $td = "<td  class='today' onclick='signDay($(this))'>
<div align='right' valign='top'><span style='position:relative;right:20px;'>" . $t . "</span></div>
<div align='center'><a style='cursor:pointer;color:#ffffff;' >签到</a></div></td>";
            } else {
                $td = "<td style='background-color:#DCDCDC;'>
<div align='right' valign='top'><span style='position:relative;right:20px;'>" . $t . "</span>
</div><div align='left'style='height:47px'>
</div></td>";
            }
        }
    }
    return $td;
}


	/*判断用户是否收藏此链接*/
	function is_collected($slink_id){
		$user_id = session('uid');
		$links = M('user_collection')->where(array('user_id'=>$user_id))->select();

		foreach ($links as $k => $v) {
			if ($slink_id == $v['slink_id']) {
				return 'collected.png';break;
				}
		}
		return 'collection.png';
	}


	/*lkl判断用户是否收藏此链接2*/
	function is_collected_res($slink_id){
		$user_id = session('uid');
		$links = M('user_collection')->where(array('user_id'=>$user_id))->select();
		$result = 0;
		foreach ($links as $k => $v) {
			if ($slink_id == $v['slink_id']) {
					$result = 1;break;
				}
		}
		return $result;
	}
	//在线交易订单支付处理函数  
    //函数功能：根据支付接口传回的数据判断该订单是否已经支付成功；  
    //返回值：如果订单已经成功支付，返回true，否则返回false； 
    // 
	function checkorderstatus($ordid){  
		$Ord=M('Orderlist');  
		$ordstatus=$Ord->where('ordid='.$ordid)->getField('ordstatus');  
		if($ordstatus==1){  
		    return true;
		}else{  
		    return false;      
		}  
	}


	//处理订单函数  
	//更新订单状态，写入订单支付后返回的数据  
	function orderhandle($parameter){  
		$ordid=$parameter['out_trade_no'];  
		$data['payment_trade_no']      =$parameter['trade_no'];  
		$data['payment_trade_status']  =$parameter['trade_status'];  
		$data['payment_notify_id']     =$parameter['notify_id'];  
		$data['payment_notify_time']   =$parameter['notify_time'];  
		$data['payment_buyer_email']   =$parameter['buyer_email'];  
		$data['ordstatus']             =1;  
		$Ord=M('Orderlist');  
		$Ord->where('ordid='.$ordid)->save($data);  
	}   
	/*----------------------------------- 
	2013.8.13更正 
	下面这个函数，其实不需要，大家可以把他删掉， 
	具体看我下面的修正补充部分的说明 
	------------------------------------*/  
	//获取一个随机且唯一的订单号；  
	function getordcode(){  
		$Ord=M('Orderlist');  
		$numbers = range (10,99);  
		shuffle ($numbers);   
		$code=array_slice($numbers,0,4);   
		$ordcode=$code[0].$code[1].$code[2].$code[3];  
		$oldcode=$Ord->where("ordcode='".$ordcode."'")->getField('ordcode');
		  
		if($oldcode){  
		    getordcode();  
		}else{  
		    return $ordcode;  
		}  
	}


	//lkl新增，自动判断用户广告和置顶过期
	function check_ad_time(){
		
		$Ad = D('s_ad');
		$Zd = D('s_zd');
		$Sl = D('slink');
		$now = time();
		//处理广告过期
		$Ad->where("end_time < $now AND type = 2")->save(array('type'=>3));
		//处理置顶过期
		$Zd->where("end_time < $now AND type = 2")->save(array('type'=>3));
		$Sl->where("zd_end_time < $now")->save(array('sort'=>0));
	}


	


