<?php
/**
 */

namespace Home\Controller;
use Think\Controller;

/**
 * Class LoginController
 * @package Home\Controller
 */
class LoginController extends Controller {
    /**
     * 用户登录
     */
    public function login()
    {
        // 判断提交方式
        if (IS_POST) {
            // 实例化Login对象
            $login = D('login');

            // 自动验证 创建数据集
            if (!$data = $login->create()) {
                // 防止输出中文乱码
                header("Content-type: text/html; charset=utf-8");
                exit($login->getError());
            }

            // 组合查询条件
            $where = array();
            $where['username'] = $data['username'];
            $where['password'] = $data['password'];

            //dump($where);
            $result = $login->where($where)->field('userid,username,password,lastdate,lastip')->find();

            // 验证用户名 对比 密码
            if ($result && $result['password'] == $result['password']) {
                // 存储session
                session('uid', $result['userid']);          // 当前用户id
                session('username', $result['username']);   // 当前用户名
                session('lastdate', date('y-m-d H:i', $result['lastdate']));   // 上一次登录时间
                session('lastip', $result['lastip']);       // 上一次登录ip

                // 更新用户登录信息
                $where['userid'] = session('uid');
                M('users')->where($where)->setInc('loginnum');   // 登录次数加 1
                M('users')->where($where)->save($data);   // 更新登录时间和登录ip

                $this->success('登录成功,正在跳转。', U('Index/index')); //不跳转到当前页  success方法的默认跳转地址是$_SERVER["HTTP_REFERER"]  一个参数表示提示信息，第二个参数表示跳转地址，第三个参数是跳转时间（单位为秒），
            } else {
                $this->error('登录失败,用户名或密码不正确!');
            }
        } else {
            $this->display();
        }
    }

    /**
     * 用户注册1  首页用户注册
     */
    public function register()
    {

//系统常用变量
//echo  $_SERVER['REQUEST_URI'];
 // $pid = $_SERVER["QUERY_STRING"];
 // dump($pid);
// echo $_SERVER['SERVER_NAME']; 
//echo dirname(__FILE__)."/../"; 
//echo I('_post.id');
//echo I('path.1'); 
        $yqm['pid'] = $_SERVER["QUERY_STRING"];
        $this->assign('yqm',$yqm);
  


        // 判断提交方式 做不同处理
        if (IS_POST) {
            // 实例化User对象
            $user = D('users');
       
                
     
           //dump($IS_POST);
            //自动验证 创建数据集
            if (!$data = $user->create()) {
                // 防止输出中文乱码
                header("Content-type: text/html; charset=utf-8");
                exit($user->getError());
            }

            //插入数据库
            if ($id = $user->add($data)) {
                  //
               
                /* 直接注册用户为超级管理员,子用户采用邀请注册的模式,
                   遂设置公司id等于注册用户id,便于管理公司用户*/
                  // $yqm['pid'] = $_SERVER["QUERY_STRING"];
                 //dump($yqm);
                // $User = M('Users');
               // $User->field('pid')->data($yqm)->add();
                if ($yqm['pid']) {
                       M('users')->where(array('userid'=>$id))->save(array('pid'=>$yqm['pid']));
                   }   
                $user->where("userid = $id")->setField('companyid', $id);
                
                /*送分*/
                $file="./App/Common/Conf/config.php"; //全局配置
                $config = include $file;
                $score = $config['REG'];
                M()->execute(" update users set integral = (integral + '$score') where userid = '$id'");
                /*记log*/
                $log_data = array(
                    'user_id'=>$id,
                    'integral'=>$score,
                    'desc'=>'注册送积分',
                    'add_time'=>time()
                );
                M('integral_log')->add($log_data);

                /*创建自定义模块*/
                $data = array(
                    'user_id'=>$id,
                    'title'=>'常用搜索',
                    'add_time'=>time(),
                    'last_update'=>time()
                );
                M('user_block')->add($data);

                $this->success('注册成功', U('Login/login'), 2);
            } else {
                $this->error('注册失败');
            }
        } else {
            $this->display();
        }
    }

    /**
     * 用户注册2  用户注册
     */




    /**
     * 用户注销
     */
    public function logout()
    {
        // 清楚所有session
        session(null);
        redirect(U('Login/login'), 2, '正在退出登录...');
    }

    /**
     * 验证码
     */
    public function verify()
    {
        // 实例化Verify对象
        $verify = new \Think\Verify();

        // 配置验证码参数
        $verify->fontSize = 20;     // 验证码字体大小
        $verify->length = 4;        // 验证码位数
        $verify->imageH = 60;       // 验证码高度
        $verify->useImgBg = false;   // 开启验证码背景
        $verify->useNoise = false;  // 关闭验证码干扰杂点 true
        $verify->imageW = 148;  // 验证码宽度 设置为0为自动计算
        $verify->entry();
    }
     /**
     * 检测验证码
     */
     function check_verify($code, $vid = 1) {
        $verify = new \Think\Verify();
        return $verify->check($code, $vid);
    }





    /**
     * 忘记密码，重置密码
     */
     public function resetPassword() {
        if (IS_POST) {

            $reg_type = I('post.reg_type');
            switch ($reg_type) {

                case 'email':
                    $username = I('post.email');
                    $condition['email'] = I('post.email');
                    $condition['email_bind'] = 1;
                    break;


                case 'mobile':
                    $username = I('post.mobile');
                    $condition['mobile'] = I('post.mobile');
                    $condition['mobile_bind'] = 1;
                    break;
        }

     }
     /**
     * 邮箱验证
     */
    // public function sendMailVerify(){
    //      // 生成验证码
    //     $reg_verify = \Org\Util\String::randString(6,1);
    //     session('reg_verify', user_md5($reg_verify, I('post.email')));

    //     // 构造邮件数据
    //     $mail_data['receiver'] = I('post.email');
    //     $mail_data['title']  = '邮箱验证';
    //     $mail_data['content'] = '你好：<br>邮箱'.I('post.email').'【注册/修改密码】，请在验证码输入框中输入：
    //     <span style="color:red;font-weight:bold;">'.$reg_verify.'</span>，以完成操作。<br>
    //     注意：此操作可能会修改您的密码、登录邮箱或绑定手机。如非本人操作，请及时登录并修改
    //     密码以保证帐户安全 （工作人员不会向您索取此验证码，请勿泄漏！)';
    //     $result = D('Addons://Email/Email')->send($mail_data);

    //     // 发送邮件
    //     if ($result) {
    //         $this->success('发送成功，请登陆邮箱查收！');
    //     } else {
    //         $this->error('发送失败！');
    //     }
    // }



      /**
     * 短信验证
     */
   /* public function sendMobileVerify() {
         // 生成验证码
        $reg_verify = \Org\Util\String::randString(6,1);
        session('reg_verify', user_md5($reg_verify, I('post.mobile')));

        // 构造短信数据
        $msg_data['receiver'] = I('post.mobile');
        $msg_data['message'] = '短信验证码：'.$reg_verify;
        $result = D('Addons://Message/Message')->sendMessage($msg_data);
        if ($result) {
            $this->success('发送成功，请查收！');
        } else {
            $this->error('发送失败！');
        }
    }
*/

}
}