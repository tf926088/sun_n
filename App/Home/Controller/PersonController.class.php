<?php
namespace Home\Controller;
use Think\Controller;
header("Content-type: text/html; charset=utf-8");
//用户信息中心，各种数据交互
class PersonController extends Controller{
	public function welcome() {
		$userinfo=D('users');
		$where=array('userid'=>session('uid'));  //uid
		$fields=array('nickname','sex','loginnum','vip','username','lastdate');
		$userinfos=$userinfo->field($fields)->where($where)->find();
		//dump($userinfos);
		$location=explode(',', $userinfos['location']);
		$ip = get_client_ip(); //获取用户目前登录IP
		$this->assign('ip',$ip);
		$this->assign('userinfos',$userinfos);
		$this->display();
	}
	/**
	 * 个人中心首页
	 */
	public function index() {
		$userinfo=D('users');
		$where=array('userid'=>session('uid'));  //uid
		$fields=array('nickname','sex','loginnum','vip','username');
		$userinfos=$userinfo->field($fields)->where($where)->find();
		$location=explode(',', $userinfos['location']);
		$ip = get_client_ip(); //获取用户目前登录IP
		$this->assign('prov',$location[0]);
		$this->assign('city',$location[1]);
		$this->assign('dist',$location[2]);
		$this->assign('ip',$ip);
		$this->assign('userinfos',$userinfos);
		$this->display();
	}
	/**
	 * 我的 资料  基本资料设置
	 */
	public function mydata() {
	   //dump($_SESSION);  //通过session判断登录状态
		$userinfo=D('users');
		//dump($userinfo);
		$where=array('userid'=>session('uid')) ;
		$fields=array('nickname','sex','username','vip','school');
		$userinfos=$userinfo->field($fields)->where($where)->find();
		$location=explode(',', $userinfos['location']);
		$this->assign('prov',$location[0]);
		$this->assign('city',$location[1]);
		$this->assign('dist',$location[2]);
		$this->assign('userinfos',$userinfos);
		$this->display();
	}
	/**
	* 我的账户
	*/
	public function account() {
	   //dump($_SESSION);  //通过session判断登录状态
		$userinfo=D('users');
		$where=array('userid'=>session('uid'));
		$fields=array('nickname','sex','vip','cmoney','username','vip','integral','creditscore','lineofcredit');
		$userinfos=$userinfo->field($fields)->where($where)->find();
		$location=explode(',', $userinfos['location']);
		$this->assign('prov',$location[0]);
		$this->assign('city',$location[1]);
		$this->assign('dist',$location[2]);
		$this->assign('userinfos',$userinfos);
		/*获得红包数，获得红包金额，发放红包数，发放红包金额，获得佣金总额*/
		$uid = session('uid');
		$log = M('user_account_log');
		$page = I("p",1,"int");
		$limit = 10;
		$data = $log->where(array('user_id'=>$uid))->order('log_id DESC')->page($page.','.$limit)->select();
		$count = $log->where(array('user_id'=>$uid))->count();
		foreach ($data as $k => $v) {
			$data[$k]['time_fmt'] = date('Y-m-d H-i-s',$v['add_time']);
			if ($v['from_user'] != 0) {
				$data[$k]['user_name'] = M('users')->where(array('userid'=>$v['from_user']))->getField('username');
			}
		}
		$Page = new \Think\Page($count,$limit);
		$page_res = $Page->show();
		$get_bonus_count = $log->where(array('user_id'=>$uid,'desc'=>'获得红包'))->count();
		$get_bonus = $log->where(array('user_id'=>$uid,'desc'=>'获得红包'))->sum('money');
		$send_bonus_count = $log->where(array('user_id'=>$uid,'desc'=>'发送红包'))->count();
		$send_bonus = $log->where(array('user_id'=>$uid,'desc'=>'发送红包'))->sum('money');
		$send_bonus *= -1;
		$commission = $log->where(array('user_id'=>$uid,'desc'=>array('like','%分销%')))->sum('money');
		$commission = $commission? $commission:0;
		$get_bonus_count = $get_bonus_count? $get_bonus_count:0;
		$get_bonus = $get_bonus? $get_bonus:0;
		$send_bonus_count = $send_bonus_count? $send_bonus_count:0;
		$commission = $commission? $commission:0;
		//lkl新增，读取用户发布的广告数量
		$adcount = D('s_ad')->where("uid=$uid")->count();
		//lkl新增，读取用户发布的置顶数量
		$zdcount = D('s_zd')->where("uid=$uid")->count();
		$eran_sum = M('user_account_log')->where($where)->sum('money');
		if (!$eran_sum) {
			$eran_sum = 0;
		}
		//我添加的搜索词数
		$add_word_count = M('cate')->where(array('userid'=>$uid))->count();
		//我收藏的链接数
		$col_link_count = M('user_collection')->where(array('user_id'=>$uid))->count();
		//我添加的链接数
		$add_link_count = M('slink')->where(array('author'=>$userinfos['username']))->count();
		$this->assign();
		$this->assign('eran_sum',$eran_sum);
		$this->assign('sub_count',$sub_count);
		$this->assign('get_bonus_count',$get_bonus_count);
		$this->assign('get_bonus',$get_bonus);
		$this->assign('send_bonus_count',$send_bonus_count);
		$this->assign('send_bonus',$send_bonus);
		$this->assign('adcount',$adcount);
		$this->assign('zdcount',$zdcount);
		$this->assign('add_word_count',$add_word_count);
		$this->assign('col_link_count',$col_link_count);
		$this->assign('add_link_count',$add_link_count);
		$this->display();
	}
	/**
	* 安全中心
	*/
	public function safe() {
	   //dump($_SESSION);  //通过session判断登录状态
		$userinfo=D('users');
		$where=array('uid'=>session('uid'));
		$fields=array('nickname','sex','vip','cmoney');
		$userinfos=$userinfo->field($fields)->where($where)->find();
		$location=explode(',', $userinfos['location']);
		$this->assign('prov',$location[0]);
		$this->assign('city',$location[1]);
		$this->assign('dist',$location[2]);
		$this->assign('userinfos',$userinfos);
		$this->display();
	}
	/**
	* 我的VIP
	*/
	public function vip() {
	   //dump($_SESSION);  //通过session判断登录状态
		$userinfo=D('users');
		//dump($userinfo);
		$where=array('userid'=>session('uid'));
		$fields=array('nickname','sex','vip','username','cmoney');
		$userinfos=$userinfo->field($fields)->where($where)->find();
		$location=explode(',', $userinfos['location']);
		$this->assign('prov',$location[0]);
		$this->assign('city',$location[1]);
		$this->assign('dist',$location[2]);
		$this->assign('userinfos',$userinfos);
		//dump($userinfos);
		$this->display();
	}
	/**
	* 我的VIP 升级
	*/
	public function vipup() {
		//通过session判断登录状态
		$userinfo=D('users');
		$where=array('userid'=>session('uid'));
		$fields=array('nickname','sex','vip','cmoney');
		$userinfos=$userinfo->field($fields)->where($where)->find();
		$cmoney = $userinfos['cmoney'];
		$level = I('level');
		$vip_key = 'VIP'.$level;
		$crt_vip_key = 'VIP'.$userinfos['vip'];
		$file="./App/Common/Conf/config.php"; //全局配置
		$config = include $file;
		$up_money = $config[$vip_key];
		$money_need = $up_money;
		if ($userinfos['vip'] > 0) {
			$money_need = $up_money - $config[$crt_vip_key];
		}
		if ($level <= $userinfos['vip']) {
			$this->error("不能低于当前的会员等级！");
		}
		if ($cmoney < $money_need) {
			$this->error("余额不足请充值！");
		}
		$money_ye = $cmoney - $money_need;
		$this->assign('userinfos',$userinfos);
		$this->assign('upmoney',$money_need);
		$this->assign('remoney',$money_ye);
		$this->assign('level',$level);
		$this->assign('dest_level',$userinfos['vip']+1);
		$this->display();
	}
	/**
	 * vip升级完成
	 */
	public function vipup_done(){
		$userinfo=D('users');
		$where=array('userid'=>session('uid'));
		$fields=array('nickname','sex','vip','cmoney','pid');
		$userinfos=$userinfo->field($fields)->where($where)->find();
		$level = I('level');
		$upmoney = I('upmoney');
		/*
		比例：分销比例+VIP比例
		 */
		/*扣除余额*/
		$new_money = $userinfos['cmoney'] - $upmoney;
		M('users')->where(array('userid'=>session('uid')))->save(array('cmoney'=>$new_money,'vip'=>$level));
		/*写入日志*/
		$data = array(
			'user_id'=>session('uid'),
			'money'=>$upmoney,
			'add_time'=>time(),
			'desc'=>'购买VIP'.$level
			);
		M('user_account_log')->add($data);
		/*分销逻辑*/
		if ($userinfos['pid'] > 0) {
			/*1级分销用户*/
			$parent_info = M('users')->where(array('userid'=>$userinfos['pid']))->find();
			/*父级用户VIP比例*/
			$sale_rate = 0;
			if ($parent_info['vip'] > 0) {
				/*拼key*/
				$vip_key = 'l1vip'.$parent_info['vip'];
				/*分销等级比例*/
				$sale_rate = M('distribution_conf')->where(array('d_key'=>$vip_key))->getField('d_value');
				$final_rate = $sale_rate;
				/*修改余额*/
				$bonus = $upmoney * $final_rate / 100;
				$new_money = $bonus + $parent_info['cmoney'];
				M('users')->where(array('userid'=>$parent_info['userid']))->save(array('cmoney'=>$new_money));
				/*记录日志*/
				$data = array(
					'user_id'=>$parent_info['userid'],
					'money'=>$bonus,
					'add_time'=>time(),
					'desc'=>'一级分销反利',
					'from_user'=>session('uid')
					);
				M('user_account_log')->add($data);
			}
			if ($parent_info['pid'] > 0) {
				/*二级分销用户*/
				$grand_parent = M('users')->where(array('userid'=>$parent_info['pid']))->find();
				/*父级用户VIP比例*/
				$vip_rate = 0;
				if ($grand_parent['vip'] > 0) {
					/*拼key*/
					$vip_key = 'l2vip'.$grand_parent['vip'];
					/*分销等级比例*/
					$vip_rate = M('distribution_conf')->where(array('d_key'=>$vip_key))->getField('d_value');
					/*比例相加*/
					$final_rate =  $vip_rate;
					/*修改余额*/
					$bonus = $upmoney * $final_rate / 100;
					$new_money = $bonus + $grand_parent['cmoney'];
					M('users')->where(array('userid'=>$grand_parent['userid']))->save(array('cmoney'=>$new_money));
					/*记录日志*/
					$data = array(
						'user_id'=>$grand_parent['userid'],
						'money'=>$bonus,
						'add_time'=>time(),
						'desc'=>'二级分销反利',
						'from_user'=>session('uid')
						);
					M('user_account_log')->add($data);
				}
			}
		}
		$this->success('升级成功',U('home/person/vip'));
	}
	//用户退出
	public function loginOut(){
		//卸载session
		session_unset();
		//销毁session
		session_destroy();
		//删除用于自动登录的cookie
		@setcookie("auto",'',time() - 3600,'/');
		$this->redirect('Index/index');
	}
	//修改信息
	public function updateInfo(){
		if(!IS_AJAX) {$this->error('非法访问！');}
		$userinfo=D('userinfo');
		$data=array(
			'location'=>I('area'),
			'job'=>I('job'),
			'nickname'=>I('nickname'),
			'sex'=>I('sex'),
			'intro'=>I('signature'),
			);
		$where=array(
			'uid'=>session('uid'),
			);
		$userinfo->where($where)->save($data);
	}
	//登陆密码修改
	public function updatepwd(){
		if(IS_POST){
			if(I('pwd')==I('pwd2') && I('pwdOld')){
				$user=D('users');
				$where=array('id'=>session('uid'));
				$oldpwd=$user->where($where)->getField('password');
				if ($oldpwd != md5(I('pwdOld'))) {
					$this->success('原密码错误！');
					return ;
				}
				$where=array('id'=>session('uid'));
				$newpwd=md5(I('pwd'));
				if($user->where($where)->save(array('password'=>$newpwd))){
					$this->success('修改密码成功！');
				}else{
					$this->error('修改密码失败！');
				}
			}else{
				$this->error('两次密码不一致！');
			}
			return;
		}
		$this->display();
	}
	//密码验证
	public function oldpwd(){
		if(!IS_AJAX) {$this->error('非法访问！');}
		$user=D('user');
		$where=array('id'=>session('uid'));
		$oldpwd=$user->where($where)->getField('password');
		if($oldpwd == md5(I('pwdOld'))){
			echo 'true';
		}else{
			echo 'false';
		}
	}
	//邮箱验证
	public function checkemail(){
		$id=session('uid');
		$userinfo=D('userinfo');
		$userinfos=$userinfo->where(array('uid'=>$id))->find();
		$this->assign('userinfos',$userinfos);
		$this->display();
	}
		 //头像界面载入
	// public function face(){
	//     $userinfo=D('userinfo');
	//     $where=array('uid'=>session('uid'));
	//     $face=$userinfo->where($where)->getField('face180');
	//     $this->assign('face',$face);
	//     $this->display();
	// }
	//     //上传头像
	//     public function upface(){
	//         $upload=$this->_uppic('Face');
	//         echo json_encode($upload);
	//     }
		// public function setface(){
		//     $userinfo=D('userinfo');
		//     $where=array('uid'=>session('uid'));
		//     $field=array('face50','face80','face180','points');
		//     $old=$userinfo->where($where)->field($field)->find();
		//     $arr=array(
		//         'face50'=>I('small'),
		//         'face80'=>I('mid'),
		//         'face180'=>I('max'),
		//         );
		//     if($userinfo->where($where)->save($arr)){
		//         if(!empty($old['face180'])){
		//             unlink($old['face180']);
		//             unlink($old['face80']);
		//             unlink($old['face50']);
		//         }else{
		//             $arr=array(
		//                 'points'=>$old['points']+C('FACE'),
		//                 );
		//             $userinfo->where($where)->save($arr);
		//         }
		//         echo 1;
		//     }else{
		//         echo 0;
		//     }
		// }
	//图片处理
	private function _uppic($path){
		$upload = new \Think\Upload();// 实例化上传类
		$upload->maxSize = 3145728;
		$upload->rootPath = './';
		$upload->savePath = './Public/Uploads/'.$path.'/';
		$upload->saveName = array('uniqid','');
		$upload->exts = array('jpg', 'gif', 'png', 'jpeg');
		$upload->autoSub  = true;
		$upload->subName  = array('date','Ym');
		if($info=$upload->upload()){
			$origin=$upload->savePath.date('Ym').'/'.$info['Filedata']['savename'];
			$max=$upload->savePath.date('Ym').'/'.'max_'.$info['Filedata']['savename'];
			$mid=$upload->savePath.date('Ym').'/'.'mid_'.$info['Filedata']['savename'];
			$small=$upload->savePath.date('Ym').'/'.'small_'.$info['Filedata']['savename'];
			$img = new \Think\Image();
			$img->open($origin);
			$img->thumb(180, 180)->save($max);
			$img->thumb(80, 80)->save($mid);
			$img->thumb(50, 50)->save($small);
			unlink($origin);
			return array(
				'status'=>1,
				'path'=>array(
					'max'=>$max,
					'mid'=>$mid,
					'small'=>$small,
					),
				);
		}else{
			return array('status'=>0,'msg'=>$upload->getError());
		}
	}
	// //用户信息详情
	// public function copy(){
	//     dump($_SESSION);
	//     $id = I("id",0,"int");
	//     $person_info =D('user')->getPersonById($id);
	//     $this->assign("person_info",$person_info);
	//     $this->display();
	// }
	//我的推广 太阳网 全网
	public function generalize(){
		$user = M("users");
		//用户信息
		$userinfos = $user->where(array('userid'=>session('uid')))->find();
		/*推荐链接*/
		$rec_url = $_SERVER['HTTP_HOST'].'/index.php/home/login/register.html'.'?'.$userinfos['userid'];
		/*分销获利和*/
		$where['user_id'] = $userinfos['userid'];
		$where['desc'] = array('in',array('一级分销反利','二级分销反利'));
		$eran_sum = M('user_account_log')->where($where)->sum('money');
		if (!$eran_sum) {
			$eran_sum = 0;
		}
		//dump($eran_sum);
		$userinfos1 = $user->where('pid='.$userinfos['userid'])->select(); //获取子分类用户和其userid
		$sub_one_count = 0;
		$sub_one_count = $user->where('pid='.$userinfos['userid'])->count(); //一级下线数
		$per = array();  //k是键名   v是值
		foreach ($userinfos1 as $k=>$v) {
			$where['user_id'] = $userinfos['userid'];
			$where['desc'] = '一级分销反利';
			$where['from_user'] = $v['userid'];
	 		$sum_get = M('user_account_log')->where($where)->sum('money');
	 		if (!$sum_get) {
	 			$sum_get = 0;
	 		}
	 		$f_where['pid'] = $v['userid'];
		 	$f_num  = $user->where($f_where)->count();
	 		$userinfos1[$k]['yj_money_get'] = $sum_get;
	 		$userinfos1[$k]['f_num'] = $f_num;
	 		$userinfos1[$k]['sub_count'] = $this->yj_sub_count($v['userid']);
			if($v['pid'] == $userinfos['userid'])
			{
				$per[]=$v['userid'];
			}
		}
		$sub_two_count = 0;
		if ($per) {
			$where1['pid'] = array('in',$per);
			$userinfos2 = $user->where($where1)->select();
			foreach ($userinfos2 as $k => $v) {
				$where['user_id'] = $userinfos['userid'];
				$where['desc'] = '二级分销反利';
				$where['from_user'] = $v['userid'];
		 		$sum_get = M('user_account_log')->where($where)->sum('money');
		 		if (!$sum_get) {
	 				$sum_get = 0;
	 			}
		 		$f_where['pid'] = $v['userid'];
		 		$f_num  = $user->where($f_where)->count();
		 		$userinfos2[$k]['yj_money_get'] = $sum_get;
		 		$userinfos2[$k]['f_num'] = $f_num;
		 		$userinfos2[$k]['sub_count'] = $this->yj_sub_count($v['userid']);
			}
			$sub_two_count = $user->where($where1)->count();
			$this->assign('sub_two_count',$sub_two_count);
		}
		$sub_count = $sub_two_count + $sub_one_count;
		$this->assign('rec_url',$rec_url);
		$this->assign('eran_sum',$eran_sum);
		$this->assign('userinfos',$userinfos);
		$this->assign('userinfos1',$userinfos1);
		$this->assign('userinfos2',$userinfos2);
		$this->assign('sub_one_count',$sub_one_count);
		$this->assign('sub_two_count',$sub_two_count);
		$this->assign('sub_count',$sub_count);
		$this->display();
	}
	/*start yjmp*/
	/**
	 * 身份证上传页面显示
	 */
	public function auth(){
		$user = M('users');
		$where=array('userid'=>session('uid'));
		$fields=array('idcard_front','idcard_back','verify_status','sex','nickname','vip','cmoney','username');
		$userinfo=$user->field($fields)->where($where)->find();
		$this->assign('userinfos',$userinfo);
		$this->display();
	}
	/**
	 * 身份证上传操作
	 */
	public function auth_act(){
		$upload = new \Think\Upload();// 实例化上传类
		$upload->maxSize   =     3145728 ;// 设置附件上传大小
		$upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		$upload->rootPath  =      './Upload/img/'; // 设置附件上传根目录
		$id_card_number  =I('idcard_number','');
		if ($_FILES['idcard_front'] && $_FILES['idcard_back']) {
		   // 上传单个文件
			$front_info   =   $upload->uploadOne($_FILES['idcard_front']);
			$back_info   =   $upload->uploadOne($_FILES['idcard_back']);
		}
		if(!$front_info || !$back_info) {// 上传错误提示错误信息
			$this->error($upload->getError());
			exit;
		}
		//上传成功写入数据库
		$user= M("users");
		$condition =array('userid'=>session('uid'));
		$data['idcard_front'] = $front_info['savepath'].$front_info['savename'];
		$data['idcard_back'] = $back_info['savepath'].$back_info['savename'];
		$data['verify_status'] = 1;
		$data['idcard'] = $id_card_number;
		$result = $user->where($condition)->data($data)->save();
		$this->success('操作完成！',U('home/person/auth'));
	}
	/**
	* 修改交易密码页面
	*/
	public function change_pwdtrade(){
		$user = M('users');
		$where=array('userid'=>session('uid'));
		$fields=array('pwdtrade','nickname','sex','vip','cmoney');
		$userinfo=$user->field($fields)->where($where)->find();
		$this->assign('userinfos',$userinfo);
		$this->display();
	}
	/**
	* 修改交易密码实现
	*/
	public function change_pwdtrade_act(){
		$pwdtrade_org = I("pwdtrade_org","");
		$pwdtrade_new = I("pwdtrade_new","");
		$pwdtrade_cfm = I("pwdtrade_new","");
		$pwd_org = trim('123456');
		$pwdtrade_org = trim($pwdtrade_org);
		if ($pwdtrade_org == '' || $pwdtrade_new == '' || $pwdtrade_cfm == '') {
			$this->error('请将数据填写完整');
		}
		/*验证用户原密码是否正确*/
		$pwd_org = '123456';
		if ($pwd_org != $pwdtrade_org) {
			$this->error('原密码错误');
		}
		if ($pwdtrade_new != $pwdtrade_cfm) {
			$this->error('两次密码输入不一致');
		}
		/*修改密码操作*/
		$user= M("users");
		$condition =array('userid'=>session('uid'));
		$data['pwdtrade'] = md5($pwdtrade_new);
		$result = $user->where($condition)->data($data)->save();
		$this->success('操作完成！',U('home/person/auth'));
	}
	/**
	* 设置交易密码
	*/
	public function set_pwdtrade(){
		$user = M('users');
		$where=array('userid'=>session('uid'));
		$fields=array('pwdtrade','nickname','sex','vip','cmoney');
		$userinfo=$user->field($fields)->where($where)->find();
		$this->assign('userinfos',$userinfo);
		$this->display();
	}
	/**
	 * 设置交易密码
	 */
	public function set_pwdtrade_act(){
		$pwdtrade = I("pwdtrade","");
		$pwdtrade_cfm = I("pwdtrade_cfm","");
		/*密码格式由js来验证*/
		if ($pwdtrade == '' || $pwdtrade_cfm == '') {
			$this->error('请将数据填写完整！');
		}
		if ($pwdtrade != $pwdtrade_cfm) {
			$this->error('两次密码输入不一致！');
		}
		//数据验证通过，写入数据库
		$user= M("users");
		$condition =array('userid'=>session('uid'));
		$data['pwdtrade'] = md5($pwdtrade);
		$result = $user->where($condition)->data($data)->save();
		$this->success('操作完成！',U('home/person/set_pwdtrade'));
	}
	/**
	 * 充钱页面
	 */
	public function pay(){
		$userinfo=D('users');
		$where=array('userid'=>session('uid'));
		$fields=array('username','nickname','sex','vip','cmoney');
		$userinfos=$userinfo->field($fields)->where($where)->find();
		$this->assign('userinfos',$userinfos);
		$this->display();
	}
	/**
	 * 充值完成
	 */
	public function pay_submit(){
		$userid = session('uid');
		$money = I("money","");
		$payment = I("payment","");
		if ($money < 0 || $money == '') {
			$this->error('输入金额不合法！');
		}
		/*支付宝在线支付*/
		if ($payment == '4') {
			$this->redirect('pay/doalipay', array('money' => $money), 1, '页面跳转中...');
		}
		switch ($payment) {
			case '1':
				$payment = '支付方式：个人支付宝转账';
				break;
			case '2':
				$payment = '支付方式：企业支付宝转账';
				break;
			case '3':
				$payment = '支付方式：公司银行账户转账';
				break;
			case '4':
				$payment = '支付方式：支付宝在线支付';
				break;
			default:
				$payment = '支付方式：默认';
				break;
		}
		$aclog = M("user_account");
		$data['user_id'] = session('uid');
		$data['amount'] = $money;
		$data['payment'] = $payment;
		$data['add_time'] = time();
		$data['process_type'] = '1';
		$res = $aclog->add($data);
		$this->success('操作完成！',U('home/person/pay'));
	}
	public function test_alipay(){
		die('233456789');
	}
	/*人民币提现页面*/
	public function monout(){
		$userinfo=D('users');
		$where=array('userid'=>session('uid'));
		$userinfos=$userinfo->where($where)->find();
		$this->assign('userinfos',$userinfos);
		$this->display();
	}
	/*人民币提现操作*/
	public function monout_act(){
		$card_number = I("card_number","");
		$money = I("money","");
		$pwdtrade = md5(I("pwdtrade",""));
		$uid = session('uid');
		$user_info =M("users")->where("userid='{$uid}'")->find();
		if ($user_info['pwdtrade'] == '') {
			$this->error('交易密码为空，请先设置交易密码');
		}
		if ($card_number == '') {
			$this->error('银行卡号不能为空！');
		}
		if (!is_numeric($money) || $money > $user_info['cmoney']) {
			$this->error('请输入正确的金额！');
		}
		if ($pwdtrade != $user_info['pwdtrade']) {
			$this->error('交易密码错误');
		}
		$aclog = M("user_account");
		$data['user_id'] = $uid;
		$data['amount'] = $money;
		$data['payment'] = $card_number;
		$data['add_time'] = time();
		$data['process_type'] = '2';
		$res = $aclog->add($data);
		$new_money = $user_info['cmoney'] - $money;
		$user_res = M('users')->where(array('userid'=>$uid))->data(array('cmoney'=>$new_money))->save();
		if ($user_res) {
			$this->success('操作完成！',U('home/person/account'));
			exit;
		}
		$this->error('操作失败！',U('home/person/account'));
	}
	/*显示用户的充值提现记录*/
	public function account_log(){
		$uid = session('uid');
		$page = I("p",1,"int");
		$limit = 10;
		$user_account = M("user_account");
		$data = M("user_account")->where(array('user_id'=>$uid))->order('log_id DESC')->page($page.','.$limit)->select();
		foreach ($data as $k => $v) {
			$data[$k]['add_time_fmt'] = date('Y-m-d H:i',$v['add_time']);
		}
		$count = $user_account->where(array('user_id'=>$uid))->count();
		$Page = new \Think\Page($count,$limit);
		$page_res = $Page->show();
		$this->assign('page',$page_res);
		$this->assign('log_list',$data);
		$this->display();
	}
	public function forgot_pwdtrade(){
		$page = 1;
		$limit = 10;
		$where['vip'] = array('gt',2);
		$user_arr = M("users")->field(array('userid','cmoney','bonus_get','vip'))->order('bonus_get ASC')->page($page.','.$limit)->where($where)->select();
		// echo "<pre>";
		// print_r($user_arr);
		// echo "</pre>";
		$this->display();
	}
	/*发红包方法*/
	public function send_bonus(){
		$money = I("money","");
		$bonus_type = I("bonus_type","");
		$number = I("number","");
		$res = $this->rand_Bonus($money,$number);
		$user_id = session('uid');
		$user_info = M('users')->where(array('userid'=>$user_id))->find();
		if (!is_numeric($money) || $money < 0) {
			$this->error('金额填写不合法');
		}
		if (!is_numeric($number) || $number < 0) {
			$this->error('人数填写不合法');
		}
		if ($user_info['cmoney'] < $money) {
			$this->error('余额不足！');
		}
		$bonus = 0;
		if ($bonus_type == 1 && ($money / $number) < 0.01) {
			$this->error('每人分得红包不得低于0.01元');
		}
		$page = 1;
		$limit = $number;
		$where['vip'] = array('gt',2);
		$user_arr = M("users")->field(array('userid','cmoney','bonus_get','vip'))->order('bonus_get ASC')->page($page.','.$limit)->where($where)->select();
		$number = count($user_arr);
		$bonus = $money / $number;
		if ($bonus_type == 1) {
			foreach ($user_arr as $k => $v) {
				$new_money = $v['cmoney'] + $bonus;
				$new_bonus_get = $v['bonus_get'] + $bonus;
				M('users')->where(array('userid'=>$v['userid']))->save(array('cmoney'=>$new_money,'bonus_get'=>$new_bonus_get));
				$data = array(
					'user_id'=>$v['userid'],
					'money'=>$bonus,
					'add_time'=>time(),
					'desc'=>'获得红包',
					'from_user'=>$user_id
					);
				M('user_account_log')->add($data);
			}
		}else{
			$tmp_money = $money * 100;
			$bonus_list = $this->rand_Bonus($tmp_money,$number);
			foreach ($user_arr as $k => $v) {
				$bonus = $bonus_list[$k] / 100;
				$new_money = $v['cmoney'] + $bonus;
				$new_bonus_get = $v['bonus_get'] + $bonus;
				M('users')->where(array('userid'=>$v['userid']))->save(array('cmoney'=>$new_money,'bonus_get'=>$new_bonus_get));
				$data = array(
					'user_id'=>$v['userid'],
					'money'=>$bonus,
					'add_time'=>time(),
					'desc'=>'获得红包',
					'from_user'=>$user_id
					);
				M('user_account_log')->add($data);
			}
		}
		/*扣除用户余额,记录日志*/
		$new_money = $user_info['cmoney'] - $money;
		$minus_money = $money * (-1);
		M('users')->where(array('userid'=>$user_id))->save(array('cmoney'=>$new_money));
		$data = array(
					'user_id'=>$user_id,
					'money'=>$minus_money,
					'add_time'=>time(),
					'desc'=>'发送红包',
					);
		M('user_account_log')->add($data);
		$this->success('红包发送成功！');
	}
	/*资金记录*/
	public function money_log(){
		$uid = session('uid');
		$log = M('user_account_log');
		$page = I("p",1,"int");
		$limit = 10;
		$data = $log->where(array('user_id'=>$uid))->order('log_id DESC')->page($page.','.$limit)->select();
		$count = $log->where(array('user_id'=>$uid))->count();
		foreach ($data as $k => $v) {
			$data[$k]['time_fmt'] = date('Y-m-d H-i-s',$v['add_time']);
			if ($v['from_user'] != 0) {
				$data[$k]['user_name'] = M('users')->where(array('userid'=>$v['from_user']))->getField('username');
			}
		}
		$Page = new \Think\Page($count,$limit);
		$page_res = $Page->show();
		/*获得红包数，获得红包金额，发放红包数，发放红包金额，获得佣金总额*/
		$get_bonus_count = $log->where(array('user_id'=>$uid,'desc'=>'获得红包'))->count();
		$get_bonus = $log->where(array('user_id'=>$uid,'desc'=>'获得红包'))->sum('money');
		$send_bonus_count = $log->where(array('user_id'=>$uid,'desc'=>'发送红包'))->count();
		$send_bonus = $log->where(array('user_id'=>$uid,'desc'=>'发送红包'))->sum('money');
		$send_bonus *= -1;
		$commission = $log->where(array('user_id'=>$uid,'desc'=>array('like','%分销%')))->sum('money');
		$commission = $commission? $commission:0;
		$get_bonus_count = $get_bonus_count? $get_bonus_count:0;
		$get_bonus = $get_bonus? $get_bonus:0;
		$send_bonus_count = $send_bonus_count? $send_bonus_count:0;
		$commission = $commission? $commission:0;
		$this->assign();
		$this->assign('get_bonus_count',$get_bonus_count);
		$this->assign('get_bonus',$get_bonus);
		$this->assign('send_bonus_count',$send_bonus_count);
		$this->assign('send_bonus',$send_bonus);
		$this->assign('commission',$commission);
		$this->assign('account_log',$data);
		$this->assign('page',$page_res);
		$this->assign_user();
		$this->display();
	}
	/*邮箱验证*/
	public function mail_check(){
	   $this->assign_user();
	   $this->display();
	}
	/*声明用户信息*/
	public function assign_user(){
		if (session('uid')) {
			$user_infos  = '';
			$where=array('userid'=>session('uid'));
			$userinfos=M('users')->where($where)->find();
			$this->assign('userinfos',$userinfos);
	   }
	}
	/**
	 * 发放随机红包
	 * @param  int $total  金额
	 * @param  int $number 数量
	 * @return array         红包发送成功！金额数组
	 */
	public function rand_Bonus($total,$number){
		$num  = $number - 1;
		$last = $total*100;
		$res = array();
		for ($i=0; $i < $number; $i++) {
				if ($i < $num) {
					//正常逻辑
					$flag = false;
					$temp = 0;
					while ($flag==false) {
					   $temp = mt_rand(1,$last/2);
					   if (($last-$temp)/($num-$i) > 1) {
							$res[$i] = $temp / 100;
							$last -= $temp;
							$flag = true;
						}
					}
				}else{
					//最后一个
					$res[$num] = $last/100;
				}
			}
		return $res;
	}
	/*end yjmp*/
	//用户诚信分  积分
	public function credit(){
		$user = M("users"); // 实例化User对象获取用户数：
		$field = array('integral','userid','vip','lineofcredit','creditscore','username');
		$where = array('userid'=>session('uid'));
		$userinfos = $user->where($where)->field($field)->find();
		//dump($userintegral);
		$this->assign('userinfos',$userinfos);
		$this->display();
	}
	//用户诚信分 诚信额度提现
	public function creditout(){
		$user = M("users"); // 实例化User对象获取用户数：
		$userintegral = $user->where('integral','userid','vip','lineofcredit','creditscore')->find();
		//dump($userintegral);
		$this->assign('userintegral',$userintegral);
		$this->display();
	}
	//会员签到
	public function sign(){
		$first = strtotime(date("Y-m-01 00:00:00"));
		$first = date("w", $first);
		$weekArr = array("日", "一", "二", "三", "四", "五", "六");
		$maxDay = date('t', strtotime("" . date("Y") . "-" . date("m") . ""));
		for ($j = 0; $j < $first; $j++) {
			$blankArr[] = $j;
		}
		for ($i = 0; $i < $maxDay; $i++) {
			$z = $first + $i;
			$days[] = array("key" => $i, "key2" => $z % 7);
		}
		$nowtime = date("Y年m月d日 H:i:s") . "&nbsp;&nbsp;星期" . $weekArr[date("w")];
		$this->assign("days", $days);
		$this->assign("first", $first);
		$this->assign("blankArr", $blankArr);
		$total = $first + count($days);
		for ($x = 0; $x < ceil($maxDay / 7) * 7 - $total; $x++) {
			$other[] = $x;
		}
		$this->assign("other", $other);
		$this->display();
	}
	//会员签到
	public function signDay() {
		$s_userid = session("uid");
		if ($s_userid == '') {
			echo -1;
			exit;
		}
		$data['addtime'] = strtotime(date("Y-m-d 00:00:00"));
		$data['uid'] = $s_userid;
		$info = M('s_sign')->field("id")->where("addtime = " . $data['addtime'] . " AND uid = " . $data['uid'] . " AND status = 0")->find();
		if (empty($info)) {
			/*获取积分*/
			$file="./App/Common/Conf/config.php"; //全局配置
			$config = include $file;
			$score = $config['SIGN'];  //签到积分
			$data['money'] = $score;
			$lastid = M('s_sign')->add($data);
			/*积分写入用户表 yjmp*/
			$res = M()->execute(" update users set integral = (integral + '$score') where userid = '$s_userid'");
			/*记录日志*/
			$log_data = array(
				'user_id'=>$s_userid,
				'integral'=>$score,
				'desc'=>'签到积分',
				'add_time'=>time()
			);
			M('integral_log')->add($log_data);
			if ($lastid > 0 && $res) {
				echo $data['money'];
			}
		} else {
			echo -1;
		}
	}
	//优惠券
	public function coupon(){
		$coupon_list = M("s_coupon")->select();
		$this->assign('coupon_list',$coupon_list);
		//dump($coupon_list);
		$this->display();
	}
	public function yj_test(){
		$file="./App/Common/Conf/config.php"; //全局配置
		$config = include $file;
		echo "<pre>";
		print_r($config['SIGN']);
		echo "</pre>";
		exit;
	}
	/*发验证邮件*/
	public function send_check_mail(){
		$mail = I("e_mail");
		$user_id = session('uid');
		$now = time() - 600;
		$condi['add_time']=array('lt'=>$now);
		$condi['user_id'] = $user_id;
		$condi['email'] = $mail;
		$count = M('mail_code')->where($condi)->count();
		if ($conun) {
			die('2');
		}
		$rand_str = yj_rand_str(4);
		$title = "太阳网邮件验证码";
		$content = "感谢您对本网站的支持，您的邮件验证码为：".$rand_str;
		if(yj_send_mail($mail,$title,$content)){
			$where = array(
				'user_id'=>session('uid')
				);
			M('mail_code')->where($where)->delete();
			$data = array(
				'code'=>$rand_str,
				'user_id'=>session('uid'),
				'add_time'=>time(),
				'email'=>$mail
				);
			M('mail_code')->add($data);
			die('success');
		}else{
			die('fail');
		}
	}
	/*验证邮件验证码*/
	public function check_mail(){
		$mail = I("e_mail");
		$code = I("check_code");
		$true_code = M('mail_code')->where(array('user_id'=>session('uid')))->find();
		if ($mail != $true_code['email']) {
			$this->error("邮箱不正确");
		}
		if ($code != $true_code['code']) {
			$this->error("验证码不正确");
		}
		/*送分*/
		$file="./App/Common/Conf/config.php"; //全局配置
		$config = include $file;
		$score = $config['MAIL_CHECK'];
		$user_id = session('uid');
		M()->execute(" update users set integral = (integral + '$score') where userid = '$user_id'");
		/*记录日志*/
		$log_data = array(
			'user_id'=>$user_id,
			'integral'=>$score,
			'desc'=>'邮箱验证积分',
			'add_time'=>time()
		);
		M('integral_log')->add($log_data);
		M('users')->where(array('userid'=>session('uid')))->save(array('email'=>$mail));
		M('mail_code')->where(array('user_id'=>$user_id))->delete();
		$this->success('邮箱验证成功');
	}
	/*积分记录*/
	public function integral(){
		$uid = session('uid');
		$page = I("p",1,"int");
		$limit = 10;
		$integral_log = M("integral_log");
		$data = M("integral_log")->where(array('user_id'=>$uid))->order('log_id DESC')->page($page.','.$limit)->select();
		foreach ($data as $k => $v) {
			$data[$k]['add_time_fmt'] = date('Y-m-d H:i',$v['add_time']);
		}
		$count = $integral_log->where(array('user_id'=>$uid))->count();
		$Page = new \Think\Page($count,$limit);
		$page_res = $Page->show();
		$this->assign('page',$page_res);
		$this->assign('log_list',$data);
		$this->display();
	}
	/*显示用户的广告记录*/
	public function myad(){
		check_ad_time();
		$uid = session('uid');
		$page = I("p",1,"int");
		$limit = 10;
		$Ad = M("s_ad");
		$data = $Ad->where(array('uid'=>$uid))->order('ad_id DESC')->page($page.','.$limit)->select();
		foreach($data as $k=>$v){
			if($v['type']==1){
				$data[$k]['last_date'] = '已下架';
			}elseif($v['type']==2){
				$data[$k]['last_date'] = $this->get_last_date($v['end_time']);
			}else{
				$data[$k]['last_date'] = '已过期';
			}
		}
		$count = $Ad->where(array('uid'=>$uid))->count();
		$Page = new \Think\Page($count,$limit);
		$page_res = $Page->show();
		$this->assign('page',$page_res);
		$this->assign('ad_list',$data);
		$this->display();
	}
	/*显示用户的置顶记录*/
	public function myzd(){
		check_ad_time();
		$uid = session('uid');
		$page = I("p",1,"int");
		$limit = 10;
		$Zd = M("s_zd");
		$data = $Zd->where(array('uid'=>$uid))->order('zd_id DESC')->page($page.','.$limit)->select();
		foreach($data as $k=>$v){
			if($v['type']==1){
				$data[$k]['last_date'] = '已下架';
			}elseif($v['type']==2){
				$data[$k]['last_date'] = $this->get_last_date($v['end_time']);
			}else{
				$data[$k]['last_date'] = '已过期';
			}
			//获取置顶信息得详情
			$sinfo = D('slink')->where("id=$v[sid]")->find();
			$data[$k]['title'] = $sinfo['title'];
			$data[$k]['img'] = $sinfo['img'];
		}
		$count = $Zd->where(array('uid'=>$uid))->count();
		$Page = new \Think\Page($count,$limit);
		$page_res = $Page->show();
		$this->assign('page',$page_res);
		$this->assign('ad_list',$data);
		$this->display();
	}
	/* 分割线上面是ACTION，下面是方法 */
	/* ********************************************** */
	/*二级下线数*/
	public function yj_sub_count($user_id){
		$user = M('users');
		$userinfos1 = $user->where('pid='.$user_id)->select();
		$sub_one_count = $user->where('pid='.$user_id)->count(); //一级下线数
		$sub_two_count = 0;
		foreach ($userinfos1 as $k=>$v) {
			if($v['pid'] == $user_id)
			{
				$per[]=$v['userid'];
			}
		}
		if ($per) {
			$where1['pid'] = array('in',$per);
			$sub_two_count = $user->where($where1)->count();
		}
		$sub_count = $sub_one_count + $sub_two_count;
		return $sub_count;
	}
	//lkl读取剩余时间
	function get_last_date($time){
		$now = time();
		$last_time = $time - $now;
		if($last_time>0){
			$day = floor($last_time/24/3600);
			$hour = floor(($last_time-$day*24*3600)/3600);
		}
		return "剩余{$day}天{$hour}小时";
	}

	public function edit_done(){
		$sex = empty($_POST['sex'])? 1 : $_POST['sex'];
		$job = empty($_POST['job'])? '' : $_POST['job'];
		$school = $_POST['school']== '0' ? '' : $_POST['school'];

		$user_id = session('uid');
		$data = array(
			'sex'=>$sex,
			'job'=>$job,
			'school'=>$school
		);
		M('users')->where(array('userid'=>$user_id))->save($data);
		$this->success('操作成功！');
	}
}