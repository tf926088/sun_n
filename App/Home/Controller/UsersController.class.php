<?php
/**
 *
 */

namespace Home\Controller;
use Think\Controller;

/**
 * 
 */
class UsersController extends Controller {
    /**
     * get users list
     */
    public function userlist()
    {
        $list = M('users')->where(array('userid' => $this->userid))->find();

        var_dump($list);
    }


      //用户信息详情
    public function detail(){

        $id = I("id",0,"int");
        $user_info =D('user')->getUserById($id);
    
        $this->assign("user_info",$user_info);

        $this->display();

    }

      public function lists(){
        $user_list = D("Users")->getUserList();
        $this->assign('user_list',$user_list);
        //dump($user_list);

        $this->display();

    }



  /**
     * 用户列表
     * 
     */
    public function index($user_type = 1) {
        // 获取用户类型的搜索字段
        /*$user_type_info = D('User/Type')->find($user_type);*/
        $con = array();
        $con['user_type'] = $user_type;
        $con['id'] = array('in', $user_type_info['list_field']);
 /*       $query_attribute = D('User/Attribute')->where($con)->select();*/
        foreach ($query_attribute as &$value) {
            $value['options'] = parse_attr($value['options']);

            // 构造搜索条件
            if ($_GET[$value['name']] !== 'all' && $_GET[$value['name']]) {
                switch ($value['type']) {
                    case 'radio':
                        $tmp = $_GET[$value['name']];
                        $map[$value['name']] = $tmp;
                        break;
                    case 'select':
                        $tmp = $_GET[$value['name']];
                        $map[$value['name']] = $tmp;
                        break;
                    case 'checkbox':
                        $tmp = $_GET[$value['name']];
                        $map[$value['name']] = array(
                            'like',
                            array(
                                $tmp,
                                $tmp.',%',
                                '%,'.$tmp.',%',
                                '%,'.$tmp
                            ),
                            'OR'
                        );
                        break;
                }
            }
        }

        // 获取用户基本信息
        $map['status']    = 1;
        $map['user_type'] = $user_type;
        $p = !empty($_GET["p"]) ? $_GET['p'] : 1;
      /*  $user_object  = D('Admin/User');*/
        $base_table   = C('DB_PREFIX').'admin_user';
        $extend_table = C('DB_PREFIX').'user_'.strtolower($user_type_info['name']);
       /* $user_list = $user_object
                   ->page($p, 16)
                   ->where($map)
                   ->order('create_time desc')
                   ->join($extend_table.' ON '.$base_table.'.id = '.$extend_table.'.uid', 'LEFT')
                   ->select();
        $page = new Page(
            $user_object
            ->where($map)
            ->join($extend_table.' ON '.$base_table.'.id = '.$extend_table.'.uid', 'LEFT')
            ->count(),
            16
        );*/

      /*  $this->assign('page', $page->show());
        $this->assign('query_attribute', $query_attribute);
        $this->assign('meta_title', '用户');
        $this->assign('user_list', $user_list);*/
        $this->display();
    }


    public function payed(){
        $this->success('支付成功！',U('home/person/welcome'));
    }

    public function unpayed(){
        $this->error("支付失败！");
    }
	
}