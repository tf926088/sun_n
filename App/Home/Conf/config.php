<?php
return array(
//'配置项'=>'配置值'
    'LANG_SWITCH_ON'     => true,   // 开启语言包功能
    'LANG_AUTO_DETECT'   => true, // 自动侦测语言 开启多语言功能后有效
    'DEFAULT_LANG'       => 'zh-cn', // 默认语言
    'LANG_LIST'          => 'zh-cn,en-us', // 允许切换的语言列表 用逗号分隔
    'VAR_LANGUAGE'       => 'l', // 默认语言切换变量

//前台路由  '配置项'=>'配置值'
	'TMPL_PARSE_STRING'  => array(

    '__STATIC__'         => __ROOT__ . '/Public/Static',
    '__FONT__'           => __ROOT__ . '/Public/fonts',
        //'__ADDONS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/Addons',
    '__IMG__'            => __ROOT__ . '/Public/Home/images',
    '__CSS__'            => __ROOT__ . '/Public/Home/css',
    '__JS__'             => __ROOT__ . '/Public/Home/js',

    '__LOGO__'           => __ROOT__ . '/Upload/logo/',
    '__SLOGO__'          => __ROOT__ . '/Upload/slink',
    '__ALOGO__'          => __ROOT__ . '/Upload/logo/alogo', //用户头像

),
	

    
);

