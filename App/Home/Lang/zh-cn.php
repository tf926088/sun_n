<?php
return array(
    'welcome'=>'欢迎来到太阳网!',
    'help'=>'帮助中心',
    'home'=>'首 页',
    'login'=>'登 录',
    'reg'=>'注 册',
    'name'=>'用户名',
    'smd'=>'生命顿',
    'jqr'=>'未来大學',
    'sun'=>'太阳网',
    'country'=>'切换国家',
    'rich'=>'全球财富排行榜',
    'news'=>'新 闻',
    'meituan'=>'美 团',
    'map'=>'地 图',
    'house'=>'最美科幻建筑',
    'nav2345'=>'2345导航',

    'custom'=>'定制版',
    'nav2345'=>'2345导航',
    
    'baiducloud'=>'百度网盘',
    'moreweb'=>'更多网站',
   
    'aboutus'=>'关于我们',
    'introduce'=>' 太阳高效搜索简介',
    'down'=>'下载',
    'mob'=>'手机版',
    'forum'=>'微论坛',
    'password'=>'密码',
    'passwordt'=>'重复密码',
    'agree'=>'我已阅读并同意',
    'official'=>'网站官方声明',
    'grade'=>'等级',
    'exit'=>'退出',

    'welcome'=>'欢迎页面',
    'stage'=>'访问前台',
    'account'=>'我的账号',
    'profile'=>'我的资料',
    'security'=>'安全中心',
    'sign'=>'签到',
    'other'=>'其他',
    'otherasset'=>'其他资产',

    'search'=>'我的搜索',
     'activity'=>'我的活动',
    'timejob'=>'我的散工',
    'literature'=>'我的文学',

    'interlocution'=>'我的问答',
    'space'=>'我的空间',

    'company'=>'我的公司',
    'credit'=>'诚信分',
    'booking'=>'网上预约',
    'contact'=>'联系我们',
    'web'=>'网页',
    ''=>'',
    ''=>'',



    

);
?>
