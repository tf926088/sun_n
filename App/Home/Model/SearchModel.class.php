<?php
namespace Home\Model;
use Think\Model;
class  SearchModel extends Model{

	//添加超链的自动验证
	protected $_validate = array(
		array('title','require','标题不能为空！',1),  // 都有时间都验证
		array('link','require','链接不能为空！',1),
		array('email','require','邮箱不能为空！',1), 
		array('content','require','内容不能为空！',1),
		array('verify','verify','验证码错误！',1,'callback',1),
		array('name','equal','搜索词已添加！',1), 
		);
	
	//添加超链验证码
	public function verify($code){
		$verify = new \Think\Verify();
		return $verify->check($code, '');



}