<?php
namespace Mobile\Controller;
use Think\Controller;
class PersonController extends CommonController {
    	public function index(){
               $userinfo=D('users');
        //dump($userinfo);
        $where=array('uid'=>session('uid'));
        $fields=array('nickname','cmoney','sex','vip');
        $userinfos=$userinfo->field($fields)->where($where)->find();
        $location=explode(',', $userinfos['location']);
        $ip = get_client_ip(); //获取用户目前登录IP
        
        $this->assign('prov',$location[0]);
        $this->assign('city',$location[1]);
        $this->assign('dist',$location[2]);
        $this->assign('ip',$ip);
        $this->assign('userinfos',$userinfos);
    

        $this->display();
    }

      //用户退出
    public function loginout(){
       
        //卸载session
        session_unset();
        //销毁session
        session_destroy();

        //删除用于自动登录的cookie
        @setcookie("auto",'',time() - 3600,'/');
        $this->redirect('Person/index');

    }


}