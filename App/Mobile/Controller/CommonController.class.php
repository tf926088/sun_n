<?php
namespace Mobile\Controller;
use Think\Controller;
class CommonController extends Controller {

	

    
    public function __construct(){
        parent::__construct();
        if(!session('id')){
            // $this->error('请先登录系统！',U('Login/login'));
        }
        if(MODULE_NAME=='Person' && CONTROLLER_NAME=='Login'){
        	return true;
        }
        if(MODULE_NAME=='Person' && CONTROLLER_NAME=='Person' && ACTION_NAME=='logout'){
        	return true;
        }

        //其他管理的权限
        //if(session('privilege')!='*' && !in_array(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME,session('privilege'))){
        	//$this->error('没有权限访问该功能！');
        //}
    }

    

}