/*function change_code(obj){
	$("#code").attr("src",verifyURL+'/'+Math.random());
	return false;
}*/
//登录验证  1为空   2为错误
var validate={name:1,pwd:1,code:1}
$(function(){
	$("#login").submit(function(){
		if(validate.name==0 && validate.pwd==0 && validate.code==0){
			return true;
		}
		//验证用户名
		$("input[name='name']").trigger("blur");
		//验证密码
		$("input[name='pwd']").trigger("blur");
		//验证验证码
		$("input[name='code']").trigger("blur");
		return false;
	})
})
$(function(){
	//验证用户名
	$("input[name='name']").blur(function(){
		var name = $("input[name='name']");
		if(name.val().trim()==''){
			name.parent().find("span").remove().end().append("<span class='error'>用户名不能为空</span>");
			return ;
		}
		$.post(CONTROL+"/checkname",{name:name.val().trim()},function(stat){
			if(stat==1){
				validate.name=0;
				name.parent().find("span").remove();
			}else{
				name.parent().find("span").remove().end().append("<span class='error'>用户不存在</span>");
			}

		})
	})
	//验证密码
	$("input[name='pwd']").blur(function(){
		var pwd = $("input[name='pwd']");
		var name=$("input[name='name']");
		if(name.val().trim()==''){
			return;
		}
		if(pwd.val().trim()==''){
			pwd.parent().find("span").remove().end().append("<span class='error'>密码不能为空</span>");
			return ;
		}
		$.post(CONTROL+"/checkpwd",{pwd:pwd.val().trim(),name:name.val().trim()},function(stat){
			if(stat==1){
				validate.pwd=0;
				pwd.parent().find("span").remove();
			}else{
				pwd.parent().find("span").remove().end().append("<span class='error'>密码错误</span>");
			}

		})
	})
	//验证验证码
	$("input[name='code']").blur(function(){
		var code = $("input[name='code']");
		if(code.val().trim()==''){
			code.parent().find("span").remove().end().append("<span class='error'>验证码不能为空</span>");
			return ;
		}
		$.post(CONTROL+"/checkcode",{code:code.val().trim()},function(stat){
			if(stat==1){
				validate.code=0;
				code.parent().find("span").remove();
			}else{
				code.parent().find("span").remove().end().append("<span class='error'>验证码错误</span>");
			}

		})
	})
})

